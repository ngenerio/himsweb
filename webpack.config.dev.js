const path = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')

export default {
  devtool: 'cheap-module-eval-source-map',
  entry: {
    vendor: ['react', 'handsontable', 'webpack-hot-middleware/client'],
    app: ['./src/js/main', 'webpack-hot-middleware/client']
  },
  output: {
    path: '/',
    filename: '[name].bundle.js',
    publicPath: 'http://localhost:5000/assets/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        include: path.join(__dirname, 'src'),
        loader: 'babel'
      },
      {
        test: /\.scss$/,
        loader: 'style!css?sourceMap!postcss!resolve-url?sourceMap!!sass?sourceMap'
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&minetype=application/font-woff&path=assets&name=[hash].[ext]'
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?path=assets&name=[hash].[ext]'
      }
    ]
  },
  postcss: [autoprefixer],
  sassLoader: {
    includePaths: [path.join(__dirname, 'node_modules/open-sans-fontface')]
  },
  resolve: {
    alias: {
      handsontable: 'handsontable/dist/handsontable.full.min.js'
    }
  }
}
