import path from 'path'
import express from 'express'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import webpackConfig from './webpack.config.dev'

const app = express()
const port = 5000
const isDev = process.env.NODE_ENV === 'development'

if (isDev) {
  const compiler = webpack(webpackConfig)

  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath,
    stats: { colors: true }
  }))
  app.use(webpackHotMiddleware(compiler))
} else {
  app.use(express.static(`${__dirname}/dist`))
}

app.use((req, res) => {
  res.sendFile(path.join(__dirname, `${isDev ? 'src' : 'dist'}/index.html`))
})

app.listen(port, (error) => {
  if (error) {
    console.error(error)
  } else {
    console.info(`==> Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`)
  }
})
