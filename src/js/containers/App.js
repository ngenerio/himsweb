import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { fetchInitData } from '../core/actions/coreActions'
import { setLocalUser } from '../account/actions/userActions'
import { toggleMenu } from '../core/actions/menuActions'
import { push } from 'react-router-redux'
import Header from './Header'
import Sidebar from '../core/components/Sidebar'
import Progress from '../core/components/Progress'
import Alert from '../core/components/Alert'

export class App extends Component {
  constructor () {
    super()
    this.handleMenuClick = this.handleMenuClick.bind(this)
  }

  redirectToLogin () {
    const { location, user, dispatch } = this.props
    if (location.pathname !== '/account/login' && !user.availableUser) {
      dispatch(push('/account/login'))
      return true
    }

    return false
  }

  componentWillMount () {
    this.redirectToLogin()
  }

  componentWillReceiveProps (nextProps) {
    const  { user, location, core, dispatch } = this.props
    if (this.redirectToLogin()) return

    if (user.availableUser && location.pathname.startsWith('/account')) {
      dispatch(fetchInitData())
    }
  }

  handleMenuClick () {
    this.props.dispatch(toggleMenu())
  }

  render () {
    var { user, menu, core, alert, dispatch, progress, location, sidebar } = this.props
    const children = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, { dispatch, core, user })
    })

    return (
      <div className="container">
        <Progress progress={progress} />
        <Alert alert={alert} dispatch={dispatch} />
        <Header handleMenuClick={this.handleMenuClick} user={user} />
        <div className="content">
          { user.availableUser ? <Sidebar menu={menu} dispatch={dispatch} sidebar={sidebar} /> : null }
          <div className="content-holder">
            <div className="content-box">
              {children}
            </div>
          </div>
        </div>
      </div>
    )
  }
}


App.propTypes = {
  user: PropTypes.object.isRequired,
  menu: PropTypes.object.isRequired,
  core: PropTypes.object.isRequired,
  alert: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired
}

export default connect((state) => {
  return {
    user: state.user,
    menu: state.menu,
    core: state.core,
    progress: state.progress,
    alert: state.alert,
    sidebar: state.sidebar
  }
})(App)
