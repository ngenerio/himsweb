import { Component, PropTypes } from 'react'
import { Link } from 'react-router'

export class Home extends Component {
  render () {
    const { core, user } = this.props
    const { facilityName, country, region, district, town } = core.general

    return (
      <div className="content-container">
      {
        user.availableUser && core.initLoaded ?
        <div>
          <div className="content-panel-title">
            <h2>
              {`${facilityName}, ${country}`}
            </h2>
          </div>
          <div className="content-content">
            <p>Click on the links below to get started</p>
            <div className="content-content--links">
              <ul className="home-links-holder">
                <li>
                  <Link to="/setups/facility">Facility Setup</Link>
                </li>
                <li>
                  <Link to="/setups/clinic">Clinic Setup</Link>
                </li>
                <li>
                  <Link to="/setups/clinics">Clinics</Link>
                </li>
                <li>
                  <Link to="/setups/user">User Account Setup</Link>
                </li>
                <li>
                  <Link to="/setups/users">Users</Link>
                </li>
                <li>
                  <Link to="/registration/patient">Patient Registration</Link>
                </li>
                <li>
                  <Link to="/setups/occupation">Occupation Setup</Link>
                </li>
                <li>
                  <Link to="/setups/region">Region Setup</Link>
                </li>
                <li>
                  <Link to="/setups/district">District Setup</Link>
                </li>
                <li>
                  <Link to="/setups/sponsor">Sponsor Setup</Link>
                </li>
                <li>
                  <Link to="/setups/serviceplace">Service Place Setup</Link>
                </li>
                <li>
                  <Link to="/servicerequest">Service Request</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
        :
        <p className="content-loading">Loading...</p>
      }
      </div>
    )
  }
}

export default Home
