import { Component, PropTypes } from 'react'
import classnames from 'classnames'
import Icon from '../core/components/Icon'

export default class Header extends Component {
  render () {
    var { handleMenuClick, user } = this.props
    var menuClassnames = classnames('header-menuicon', {
      'is-hidden': !user.availableUser
    })

    return (
      <div className="header">
        <div className="header-titlebox">
          {
            <div className={menuClassnames}  onClick={this.props.handleMenuClick}>
              <Icon size="small" name="bars" />
            </div>
          }
          <div className="header-title">
            <p>HIMS</p>
          </div>
        </div>
        <div className="header-nav">

        </div>
        {user.availableUser &&
          <div className="header-navright">
            <span className="header-globalicon">
              <Icon size="small" name="globe" />
            </span>
            <span className="header-notificationicon">
              <Icon size="small" name="bell-o" />
            </span>
            <span className="header-usertitle">{user.User.FirstName}</span>
            <ul className="header-navright-dropdown">
              <li>Switch User</li>
              <li>Logout</li>
            </ul>
          </div>
        }
      </div>
    )
  }
}

Header.propTypes = {
    handleMenuClick: PropTypes.func.isRequired,
    user: PropTypes.object
}
