import http from '../../libs/http'
import { alert } from '../actions/alertActions'
import {
  RECEIVE_PATIENT_FORM,
  UPDATE_PATIENT_FORM,
  CLEAR_PATIENT_FORM,
  SAVE_PATIENTS_CONTACTS
} from '../constants'

export function getPatientFormData() {
  return (dispatch, getState) => {
    return http.get('Patients/FormData', {}, { uiProgress: true })
    .then((response) => {
      const { Data: [patientFormData] } = response.data
      dispatch({ type: RECEIVE_PATIENT_FORM, payload: patientFormData })
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error fetching Patient Form Data'
      }))
      return err
    })
  }
}

export function updatePatientForm(val) {
  return {
    type: UPDATE_PATIENT_FORM,
    payload: val
  }
}

export function clearPatientForm() {
  return { type: CLEAR_PATIENT_FORM }
}

export function savePatientContactInfo(contacts) {
  return { type: SAVE_PATIENTS_CONTACTS, payload: contacts }
}
