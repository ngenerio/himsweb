import { SHOW_ALERT, HIDE_ALERT } from '../constants'

export function alert(payload) {
  return {
    type: SHOW_ALERT,
    payload
  }
}

export function hideAlert() {
  return { type: HIDE_ALERT }
}
