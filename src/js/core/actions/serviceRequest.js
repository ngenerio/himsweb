import { createAction } from '../../libs/utils'
import * as ActionTypes from '../constants'
import http from '../../libs/http'
import { alert } from '../actions/alertActions'

export function getFormData () {
  return (dispatch, getState) => {
    return http.get('ServiceRequest/FormData', {}, { uiProgress: true })
    .then((response) => {
      const { Data } = response.data
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error! Could not fetch form data.'
      }))
      return err
    })
  }
}

export function saveForm() {
  return (dispatch, getState) => {

  }
}

export function updateForm(payload) {
  return createAction(ActionTypes.UPDATE_SERVICE_REQUEST, payload)
}
export function saveRequestedServices(payload) {
  return createAction(ActionTypes.SAVE_REQUESTEED_SERVICES, payload)
}

export function clearForm() {
  return createAction(ActionTypes.CLEAR_SERVICE_REQUEST)
}
