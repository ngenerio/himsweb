import http from '../../libs/http'
import * as utils from '../../libs/utils'
import {
  showProgress,
  hideProgress
} from '../actions/progressActions'
import { alert } from './alertActions'
import {
  SET_ALL_SETUPS,
  SET_ALL_CLINICS,
  SET_ALL_DIRECTORATES,
  SET_FACILITY,
  SET_GENERAL_INFORMATION,
  INIT_LOADED,
  RECEIVE_MENUS
} from '../constants'

export function setGeneralSetups(setups) {
  return {
    type: SET_ALL_SETUPS,
    payload: { setups }
  }
}

export function setClinics(clinics) {
  return {
    type: SET_ALL_CLINICS,
    payload: { clinics }
  }
}

export function setGeneralInformation() {
  return {
    type: SET_GENERAL_INFORMATION
  }
}

export function setDirectorates(directorates) {
  return {
    type: SET_ALL_DIRECTORATES,
    payload: { directorates }
  }
}

export function setFacility(facility) {
  return {
    type: SET_FACILITY,
    payload: { facility }
  }
}

export function initLoaded() {
  return { type: INIT_LOADED }
}

export function fetchInitData() {
  return (dispatch) => {
    dispatch(showProgress())
    return Promise.all([
      http.get('GeneralSetups/GetGeneralSetups'),
      http.get('ClinicSetups/GetClinics'),
      http.get('ClinicSetups/GetDirectorates'),
      http.get('Facility/GetAll'),
      http.get('Menus/Get')
    ])
    .then((responses) => {
      const [
        { data: { Data: [ AllSetups ] } },
        { data: { Data: Clinics } },
        { data: { Data: Directorates } },
        { data: { Data: [ Facility ] } },
        { data: menus }
      ] = responses

      dispatch(hideProgress())

      // set these general information on the :core
      dispatch(setGeneralSetups(AllSetups))
      dispatch(setClinics(Clinics))
      dispatch(setDirectorates(Directorates))
      dispatch(setFacility(Facility))
      dispatch(setGeneralInformation())
      dispatch(initLoaded())
      dispatch({ type: RECEIVE_MENUS, payload: utils.getAllMenus(menus) })
    }, (errorResponse) => {
      dispatch(alert({
        type: 'error',
        message: 'Could not fetch data'
      }))
      dispatch(hideProgress())
    })
  }
}
