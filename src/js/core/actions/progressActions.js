import {
  SHOW_LOADING,
  HIDE_LOADING
} from '../constants'

export function showProgress() {
  return {
    type: SHOW_LOADING,
    payload: {
      show: true,
      state: 'show'
    }
  }
}

export function hideProgress() {
  return {
    type: HIDE_LOADING,
    payload: {
      show: false,
      state: 'hide'
    }
  }
}
