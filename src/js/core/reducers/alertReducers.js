import { SHOW_ALERT, HIDE_ALERT } from '../constants'

const initState = {
  title: '',
  message: '',
  type: '',
  show: false
}

export default function alertReducers(state = initState, action) {
  switch (action.type) {
    case SHOW_ALERT:
      return { ...action.payload, show: true }
    case HIDE_ALERT:
      return { ...state, show: false }
    default:
      return state
  }
}
