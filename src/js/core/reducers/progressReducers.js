import {
  SHOW_LOADING,
  HIDE_LOADING
} from '../constants'

const initState = {
  show: false,
  state: 'done'
}

export default function(state = initState, action) {
  switch (action.type) {
    case SHOW_LOADING:
    case HIDE_LOADING:
      return { ...state, show: action.payload.show, state: action.payload.state }
    default:
      return initState
  }
}
