import {
  RECEIVE_PATIENT_FORM,
  UPDATE_PATIENT_FORM,
  CLEAR_PATIENT_FORM,
  SAVE_PATIENTS_CONTACTS
} from '../constants'

const initState = {
  form: {},
  formData: {}
}

export default function (state = initState, action) {
  switch (action.type) {
    case RECEIVE_PATIENT_FORM:
      return { ...state, formData: action.payload, form: action.payload.Patient }

    case UPDATE_PATIENT_FORM:
      return {
        ...state,
        form: { ...state.form, [action.payload.field]: action.payload.value }
      }

    case CLEAR_PATIENT_FORM:
      return {
        ...state,
        form: { }
      }

    case SAVE_PATIENTS_CONTACTS:
      return {
        ...state,
        form: { ...state.form, Contacts: action.payload }
      }

    default:
      return state
  }
}
