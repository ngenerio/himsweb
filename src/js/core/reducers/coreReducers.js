import _ from 'lodash'
import {
  SET_ALL_SETUPS,
  SET_ALL_CLINICS,
  SET_ALL_DIRECTORATES,
  SET_FACILITY,
  SET_GENERAL_INFORMATION,
  INIT_LOADED
} from '../constants'

const initState = {
  clinics: {},
  directorates: {},
  setups: {},
  facility: {},
  general: {
    facilityName: '',
    country: '',
    region: '',
    district: '',
    town: ''
  },
  initLoaded: false
}

function setGeneral(state) {
  const { facility: { RegionID, TownID, DistrictID, CountryID }, setups } = state
  const { Name: country } =  _.find(setups.Countries, { Id: CountryID }) || { Name: '' }
  const { Name: region } = _.find(setups.Regions, { Id: RegionID }) || { Name: '' }
  const { Name: district } = _.find(setups.Districts, { Id: DistrictID }) || { Name: '' }
  const { Name: town } = _.find(setups.Towns, { Id: TownID }) || { Name: '' }

  return {
    ...state,
    general: {
      facilityName: state.facility.Name,
      country,
      region,
      district,
      town
    }
  }
}

export default function coreReducers(state = initState, action) {
  switch (action.type) {
    case SET_ALL_SETUPS:
      return { ...state, setups: action.payload.setups }
    case SET_ALL_DIRECTORATES:
      return { ...state, directorates: action.payload.directorates }
    case SET_ALL_CLINICS:
      return { ...state, clinics: action.payload.clinics }
    case SET_FACILITY:
      return { ...state, facility: action.payload.facility }
    case SET_GENERAL_INFORMATION:
      return setGeneral(state)
    case INIT_LOADED:
      return { ...state, initLoaded: true }
    default:
      return state
  }
}
