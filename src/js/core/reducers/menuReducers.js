import { TOGGLE_MENU } from '../constants'

export const initState = {
  show: true
}

export default function menuReducer(state = initState, action) {
  switch (action.type) {
    case TOGGLE_MENU:
      return { show: !state.show }
    default:
      return state
  }
}
