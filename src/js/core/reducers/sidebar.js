import * as actionTypes from '../constants'

const initState = {
  menus: []
}

export default function (state = initState, action) {
  switch (action.type) {
    case actionTypes.RECEIVE_MENUS:
      return { ...state, menus: action.payload }
    default:
      return state
  }
}
