import { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import Icon from './Icon'

export default class MenuItem extends Component {
  render () {
    var { label, link } = this.props

    return (
      <li className="sidebar-menuitem">
        <Icon name="circle-thin" size="small" />
        <Link to={link}>{label}</Link>
      </li>
    )
  }
}

MenuItem.PropTypes = {
    label: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired
}
