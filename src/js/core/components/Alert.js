import { Component, PropTypes } from 'react'
import classnames from 'classnames'
import { hideAlert } from '../actions/alertActions'

export default class Alert extends Component {
  constructor () {
    super()
    this.timer = 0
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.alert.show) {
      clearTimeout(this.timer)
    }
  }

  componentDidUpdate () {
    const { show } = this.props.alert

    if (show) {
      this.timer = setTimeout(() => this.props.dispatch(hideAlert()), 4000)
    }
  }

  render () {
    const { type, message, show } = this.props.alert
    const classNames = classnames('alert-content', {
      success: type === 'success',
      error: type === 'error'
    })
    const active = classnames('alert-container', show ? 'is-active' : 'is-inactive')

    return (
      <div className={active}>
        <div className={classNames}>
          <p>{message}</p>
        </div>
      </div>
    )
  }
}

Alert.propTypes = {
    alert: PropTypes.object.isRequired
}
