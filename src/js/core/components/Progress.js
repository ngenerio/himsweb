import { Component, PropTypes } from 'react'
import NProgress from 'nprogress'

export default class Progress extends Component {
  componentDidMount () {
    NProgress.configure({ parent: 'body' })
  }

  componentWillReceiveProps (nextProps) {
    const { progress } = this.props

    if (progress.show !== nextProps.progress.show) {
      if (nextProps.progress.show) {
        NProgress.start()
      } else {
        NProgress.done()
      }
    }
  }

  render () {
    return (<div/>)
  }
}

Progress.propTypes = {
  loading: PropTypes.object
}
