import { Component, PropTypes } from 'react'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import Icon from './Icon'

export default class InputWrapperWithDate extends Component {
  render () {
    const { label, name, value, handleChange, errors } = this.props

    return (
      <div className="input-wrapper-container">
        <div className="input-wrapper">
          <div className="input-icon">
            <Icon size="small" name="calendar-times-o" />
          </div>
          <DatePicker selected={value ? moment(value) : moment()} onChange={handleChange} placeholderText={label} />
        </div>
        {errors && errors[name] ?
          <div className="error">{ errros[name] }</div> :
          null
        }
      </div>
    )
  }
}

InputWrapperWithDate.propTypes = {
  handleChange: PropTypes.func,
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any
}
