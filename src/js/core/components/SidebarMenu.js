import { Component, PropTypes } from 'react'
import classnames from 'classnames'
import Icon from './Icon'

class SidebarMenu extends Component {
  constructor () {
    super()

    this.handleClik = this.handleClick.bind(this)
    this.state = {
      displayLinks: false,
      direction: 'up'
    }
  }

  handleClick () {
    this.setState({
      displayLinks: !this.state.displayLinks,
      direction : !this.state.displayLinks ? 'down' : 'up'
    })
  }

  render () {
    var { children, label, icon } = this.props
    var { direction, displayLinks } = this.state
    var className = classnames(displayLinks ? 'accordion-in is-active' : 'accordion-out is-collapsed')
    var directionalClass = classnames({
      slide: direction === 'down'
    })

    return (
      <li className="sidebar-navmenu">
        <div className="sidebar-navcontent-box" onClick={this.handleClik}>
          <span className={`sidebar-navicon-open sidebar-nav--inline ${directionalClass}`}>
            <Icon name="chevron-right" size="small" />
          </span>
          <span className="sidebar-navicon sidebar-nav--inline">
            <Icon name={icon} size="small" />
          </span>
          <span className="sidebar-navcontent sidebar-nav--inline">{label}</span>
        </div>
        <div className={`sidebar-navmenu-content ${className}`}>
          <ul className="sidebar-navmenu-contentbox">
            { children }
          </ul>
        </div>
      </li>
    )
  }
}

SidebarMenu.propTypes = {
  label: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
}

export default SidebarMenu
