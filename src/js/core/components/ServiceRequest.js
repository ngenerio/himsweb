import { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import {
  getFormData,
  save,
  updateForm,
  saveRequestedServices,
  clearForm
} from '../actions/serviceRequest'
import InputWrapper from './InputWrapper'
import InputWrapperWithSelect from './InputWrapperWithSelect'

export class ServiceRequest extends Component {
  constructor () {
    super()
    this.handleInputChange = this.handleInputChange.bind(this)
    this.save = (evt) => {
      evt.preventDefault()
      this.props.dispatch(save())
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearForm())
    }

    this.goToAllClinics = (evt) => {
      evt.preventDefault()
      this.props.dispatch(push('/setups/clinics'))
    }
  }

  handleInputChange (evt) {
      const target = evt.target
      let { name: field, value } = target

      if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
          value = value === 'true' ? true : false
      }

      this.props.dispatch(updateForm({field, value}))
  }

  componentDidMount () {
    const { serviceRequest } = this.props
    const getValues = (val) => {
        return { label: val.Name, value: val.Id }
    }

    this.props.dispatch(getFormData())
    .then((resp) => {
        const { serviceRequest } = this.props

        if (resp.code !== 0) return

        this.setState({
          patientCategories:  serviceRequest.formData.PatientCategories.map(getValues),
          clinics: serviceRequest.formData.Clinics.map(getValues),
          directorates: serviceRequest.formData.Directorates.map(getValues),
          patientStatuses: serviceRequest.formData.PatientStatuses.map(getValues),
          serviceRequestTypes:  serviceRequest.formData.ServiceRequestTypes.map(getValues),
          servicePlaces: serviceRequest.formData.ServicePlaces,
          currencies: serviceRequest.formData.Currencies.map(getValues),
          yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No' }]
        })

        const handsontableOptions = {
          colHeaders: ['Description', 'Default'],
          startRows: 2,
          maxRows: 10,
          minSpareRows: 1,
          columns: [
              { type: 'autocomplete', source: clinic.types.map((val) => val.Name) },
              { type: 'checkbox' }
          ]
        }

        const attendanceGrid = new Handsontable(this.refs.attendanceGrid, handsontableOptions)
        attendanceGrid.addHook('afterChange', (changes) => {
          let data = attendanceGrid.getData()

          data = data.map((val) => {

          })
          this.props.dispatch(saveRequestedServices(data))
        })
        $('.htCore').css('width', '930px')
        $('.wtHider').css('width', '930px')
      })
    }

  render () {
    const {
        clinicTypes,
        genderGroups,
        ageGroups,
        opdnumbering,
        directorates,
        patientTypes,
        servicePointTypes,
        yesAndNo
    } = this.state || {}
    const { serviceRequest } = this.props

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
            ServiceRequest Setup
          </h2>
        </div>
        <div className="content-content">
          <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Name"
                  name="Name"
                  value={serviceRequest.form.Name} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Prefix"
                  name="Prefix"
                  value={serviceRequest.form.Prefix} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Start OPD #"
                  name="StartOPDNo"
                  value={serviceRequest.form.StartOPDNo} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Digits After Prefix"
                  name="DigitsNo"
                  value={serviceRequest.form.DigitsNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Year Part Digits"
                  name="YearDigitsNo"
                  value={serviceRequest.form.YearDigitsNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="calendar-o"
                  label="Age Group"
                  name="AgeGroupID"
                  value={serviceRequest.form.AgeGroupID} options={ageGroups} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="mars"
                  label="Gender Group"
                  name="GenderGroupID"
                  value={serviceRequest.form.GenderGroupID} options={genderGroups} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="h-square"
                  label="Patient Type"
                  name="PatientStatusID"
                  value={serviceRequest.form.PatientStatusID} options={patientTypes} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="building"
                  label="Directorate"
                  name="DirectorateID"
                  value={serviceRequest.form.DirectorateID} options={directorates} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="When should patient be assigned OPD #"
                  name="ClinicOPDNoSystemID"
                  value={serviceRequest.form.ClinicOPDNoSystemID} options={opdnumbering} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="thumbs-o-up"
                  label="Active"
                  name="IsActive"
                  value={serviceRequest.form.IsActive} options={yesAndNo} />
              </div>
              <h4>Attendance Types</h4>
              <div className="input-wrapper-divider"></div>
              <div className="grid-container">
                <div className="input-wrapper-group grid-box" ref="attendanceGrid"></div>
              </div>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                    <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                    <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                 <div className="input-wrapper-container">
                    <button className="form-button" onClick={this.goToAllClinics}>View All Clinics</button>
                </div>
                <div className="input-wrapper-container">
                    <button className="form-button">Consulting Room</button>
                </div>
                <div className="input-wrapper-container">
                    <button className="form-button">External Clinics</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default connect((state) => {
  return {
    serviceRequest: state.serviceRequest
  }
})(ServiceRequest)
