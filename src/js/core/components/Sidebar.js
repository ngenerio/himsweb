import { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import classnames from 'classnames'
import SidebarMenu from './SidebarMenu'
import MenuItem from './MenuItem'
import * as sidebarActions from '../actions/sidebar'

class Sidebar extends Component {
  componentDidMount () {
    try {
      $('.nano').nanoScroller()
    } catch (e) {}
  }

  componentWillUnmount () {
    try {
      $('.nano').nanoScroller('destroy')
    } catch (e) {}
  }

  render () {
    const { show } = this.props.menu
    const { sidebar: { menus } } = this.props
    const classes = classnames('nano sidebar', show ? 'is-active': 'is-inactive')

    console.log(menus)

    return (
      <div className={classes}>
        <div className="nano-content">
          <div className="sidebar-title">
            <p className="sidebar-titlecontent">Navigation</p>
          </div>
          <ul className="sidebar-navcontainer">
            <SidebarMenu label="Daily Attendance" icon="file-archive-o">
              <MenuItem label="Patient Registeration" link="world" />
              <MenuItem label="Service Request" link="world" />
            </SidebarMenu>
            <SidebarMenu label="System Setup" icon="cogs">
              <MenuItem label="Health Facility Setup" link="setups/facility" />
              <MenuItem label="User Account" link="/setups/account" />
              <MenuItem label="Patient Category" link="/setups/patient" />
              <MenuItem label="Region" link="/setups/region" />
              <MenuItem label="Country" link="/setups/country" />
              <MenuItem label="District" link="/setups/district" />
              <MenuItem label="Service Place" link="/setups/serviceplace" />
              <MenuItem label="Clinic" link="/setups/clinic" />
              <MenuItem label="Marital Status" link="/setups/maritalstatus" />
              <MenuItem label="Occupation" link="/setups/occupation" />
              <MenuItem label="Religion" link="/setups/religion" />
            </SidebarMenu>
            <SidebarMenu label="Daily Measurements" icon="file-archive-o">
              <MenuItem label="General Vital Signs" link="world" />
              <MenuItem label="Eye Basic Vital Signs" link="world" />
              <MenuItem label="Eye Examination" link="world" />
              <MenuItem label="Antenatal" link="world" />
              <MenuItem label="Previous/ Exsinting Medical" link="world" />
              <MenuItem label="Conditions and Allergies" link="world" />
            </SidebarMenu>
          </ul>
        </div>
      </div>
    )
  }
}

Sidebar.propTypes = {
    menu: PropTypes.object
}

export default Sidebar
