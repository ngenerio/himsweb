import { Component, PropTypes } from 'react'
import cx from 'classnames'
import Icon from './Icon'

export default class InputWrapperWithSelect extends Component {
  render () {
    const { label, name, value, options = [], handleChange, icon } = this.props
    const optionElems = options.map((val, index) => {
      return (
        <option key={index} value={val.value}>{val.label}</option>
      )
    })

    const valueToString = value && value.toString()
    const newValue = (valueToString === 'true' || valueToString === 'false')
      ? valueToString : value
    let val = value
    let type = (name === 'IsActive') ? true : false

    return (
      <div className="input-wrapper-container">
        <div className="input-wrapper">
          {icon &&
          <div className="input-icon">
            <Icon size="small" name={icon} />
          </div>
          }
          <select className={cx(type ? 'input-small' : '')} name={name} value={newValue} onChange={handleChange}>
            <option>{label}</option>
            {optionElems}
          </select>
        </div>
      </div>
    )
  }
}

InputWrapperWithSelect.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any,
  selectvalues: PropTypes.array
}
