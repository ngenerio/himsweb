import { Component, PropTypes } from 'react'
import classnames from 'classnames'

export default class Icon extends Component {
  render () {
    var { size = 'small', name } = this.props
    var classes = classnames(size === 'small' ? 'fa' : 'fa-lg', `fa-${name}`)

    return (
      <i className={classes}></i>
    )
  }
}

Icon.propTypes = {
  size: PropTypes.string,
  name:  PropTypes.string.isRequired
}
