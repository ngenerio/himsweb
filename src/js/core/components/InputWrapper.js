import { Component, PropTypes } from 'react'
import cx from 'classnames'
import Icon from './Icon'

export default class InputWrapper extends Component {
  render () {
    const {
      label,
      name,
      value,
      handleChange,
      errors,
      icon
    } = this.props

    let val = value
    let type = ((typeof ++val === 'number' && val !== '') || name === 'Age' || name === 'Tdob') ? true : false

    return (
      <div className="input-wrapper-container">
        <div className="input-wrapper">
          {icon &&
          <div className="input-icon">
              <Icon size="small" name={icon} />
          </div>
          }
          <input className={cx(!type ? 'input-small' : '')} type="text" name={name} value={value} onChange={handleChange} placeholder={label}/>
        </div>
        {errors && errors[name] ?
          <div className="error">{ errros[name] }</div> :
          null
        }
      </div>
    )
  }
}

InputWrapper.propTypes = {
  handleChange: PropTypes.func,
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any
}
