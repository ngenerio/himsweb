import { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import moment from 'moment'
import {
  getPatientFormData,
  updatePatientForm,
  clearPatientForm,
  savePatientContactInfo
} from '../actions/patientActions'
import InputWrapper from './InputWrapper'
import InputWrapperWithSelect from './InputWrapperWithSelect'
import InputWrapperWithDate from './InputWrapperWithDate'

export class PatientRegistration extends Component {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleDateChange = this.handleDateChange.bind(this)
    this.handleAgeChange = this.handleAgeChange.bind(this)

    this.save = (evt) => {
      evt.preventDefault()
      this.props.dispatch(savePatient(this.props.patient))
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearPatientForm())
    }

    this.handleDelete = (evt) => {
      evt.preventDefault()
    }
  }

  handleAgeChange (evt) {
    const target = evt.target
    let { name: field, value } = target
    let age = moment().year() - Number(value)
    age = moment().set('year', age)
    age.set('month', 0)
    age.set('date', 1)

    this.props.dispatch(updatePatientForm({ field: 'Tdob', value: age }))
  }

  handleDateChange (name) {
    return (date) => {
      this.props.dispatch(updatePatientForm({field: name, value: date}))
    }
  }

  handleInputChange (evt) {
    const target = evt.target
    let { name: field, value } = target

    if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
      value = value === 'true' ? true : false
    }

    this.props.dispatch(updatePatientForm({field, value}))
  }

  componentDidMount () {
    const getValues = (val) => {
        return { label: val.Name, value: val.Id }
    }

    this.props.dispatch(getPatientFormData())
    .then((resp) => {
      if (resp.code === 1) return

      const { formData } = this.props.patient
      this.setState({
        genderGroups: formData.GenderGroups.map(getValues),
        titles: formData.Titles.map(getValues),
        maritalStatuses: formData.MaritalStatuses.map(getValues),
        patientStatuses: formData.PatientStatuses.map(getValues),
        nationalities:  formData.Nationalities.map(getValues),
        categoryTypes: formData.CategoryTypes.map(getValues),
        occupations: formData.Occupations.map(getValues),
        eduLevels: formData.EducationalLevels.map(getValues),
        regions: formData.Regions.map(getValues),
        districts: formData.Districts.map(getValues),
        towns: formData.Towns.map(getValues),
        identificationTypes: formData.IdentificationTypes.map(getValues),
        religions: formData.Religions.map(getValues),
        denominations: formData.Denominations.map(getValues),
        ethnicities: formData.Ethnicities.map(getValues),
        clinics: formData.Clinics.map(getValues),
        directorates: formData.Directorates.map(getValues),
        sponsors: formData.Sponsors.map(getValues),
        patientRelations: formData.PatientRelations.map(getValues),
        contactTypes: formData.ContactTypes.map(getValues),
        yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}]
      })

      const handsontableOptions = {
        colHeaders: [
          'Contact Name', 'Relation', 'Home Address',
          'Work Address', 'Home Phone #', 'Work/Cell Phone #',
          'Email Address'],
        startRows: 2,
        maxRows: 10,
        columns: [
          {},
          { type: 'autocomplete', source: formData.PatientRelations.map((val) => val.Name)},
          {}, {}, {}, {}, {} ]
      }

      const contactGrid = new Handsontable(this.refs.contactGrid, handsontableOptions)
      contactGrid.addHook('afterChange', (changes) => {
        let data = contactGrid.getData()
        let rows = contactGrid.countRows()

        if (changes[0][0] === rows - 1) {
          contactGrid.alter('insert_row')
        }

        data = data.filter((val) => !!val[1]).map((val) => {
          let relation = _.find(formData.PatientRelations, { Name: val[1] }) || { Id: null }
          return {
            Name: val[0],
            RelationID: relation.Id,
            HomeAddress: val[2],
            WorkAddress: val[3],
            HomeTelNo: val[4],
            WorkTelNo: val[5],
            EmailAddress: val[6]
          }
        })
        this.props.dispatch(savePatientContactInfo(data))
      })

      $('.htCore').css('width', '930px')
      $('.wtHider').css('width', '930px')
    })
  }

  render () {
    const {
      patient
    } = this.props
    const {
      genderGroups,
      titles,
      maritalStatuses,
      patientStatuses,
      nationalities,
      categoryTypes,
      occupations,
      eduLevels,
      regions,
      districts,
      towns,
      identificationTypes,
      religions,
      denominations,
      ethnicities,
      clinics,
      directorates,
      sponsors,
      patientRelations,
      contactTypes,
      yesAndNo
    } = (this.state || {})

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
            Patient Registration
          </h2>
        </div>
        <div className="content-content">
          <div className="form-content">
            <form onSubmit={this.save}>
              <h4>Demographics</h4>
              <div className="input-wrapper-divider"></div>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Surname"
                  name="LastName"
                  value={patient.form.LastName} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="First Name"
                  name="FirstName"
                  value={patient.form.FirstName} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Middle Name"
                  name="MiddleName"
                  value={patient.form.MiddleName} />
                <InputWrapperWithDate
                  handleChange={this.handleDateChange("Tdob")}
                  label="Date of Birth"
                  name="Tdob"
                  value={patient.form.Tdob} />
                <InputWrapper
                  handleChange={this.handleAgeChange}
                  icon="calendar-times-o"
                  label="Age"
                  name="Age"
                  value={patient.form.Age} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="mars"
                  label="Gender Group"
                  name="GenderGroupID"
                  value={patient.form.GenderGroupID}
                  options={genderGroups} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="life-ring"
                  label="Married"
                  name="MaritalStatusID"
                  value={patient.form.MaritalStatusID}
                  options={maritalStatuses} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="quote-left"
                  label="Title"
                  name="TitleID"
                  value={patient.form.TitleID}
                  options={titles} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="laptop"
                  label="Occupation"
                  name="OccupationID"
                  value={patient.form.OccupationID}
                  options={occupations} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Old OPD No"
                  name="OldOPDNo"
                  value={patient.form.OldOPDNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="globe"
                  label="Nationality"
                  name="NationalityID"
                  value={patient.form.NationalityID}
                  options={nationalities} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="home"
                  label="Category"
                  name="PatientCategoryTypeID"
                  value={patient.form.PatientCategoryTypeID}
                  options={categoryTypes} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="book"
                  label="Religion"
                  name="ReligionID"
                  value={patient.form.ReligionID}
                  options={religions} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="map-marker"
                  label="Home Region"
                  name="HomeRegionID"
                  value={patient.form.HomeRegionID}
                  options={regions} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="map-marker"
                  label="Home District"
                  name="HomeDistrictID"
                  value={patient.form.HomeDistrictID}
                  options={districts} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="map-marker"
                  label="Home Town"
                  name="HomeTownID"
                  value={patient.form.HomeTownID}
                  options={towns} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="map-pin"
                  label="Home Street"
                  name="HomeStreet"
                  value={patient.form.HomeStreet} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="ID Type"
                  name="IdentificationTypeID"
                  value={patient.form.IdentificationTypeID}
                  options={identificationTypes} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="ID #"
                  name="IDNumber"
                  value={patient.form.IDNumber} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="globe"
                  label="Preferred Language"
                  name="PreferredLanguageID"
                  value={patient.form.PreferredLanguageID}
                  options={yesAndNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="book"
                  label="Educational Level"
                  name="EducationalLevelID"
                  value={patient.form.EducationalLevelID}
                  options={eduLevels} />
              </div>
              <h4>Contact Information</h4>
              <div className="input-wrapper-divider"></div>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="phone"
                  label="Home Telephone Number"
                  name="HomeTelNo"
                  value={patient.form.HomeTelNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="phone"
                  label="Work Telephone Number"
                  name="WorkTelNo"
                  value={patient.form.WorkTelNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="mobile"
                  label="Cell Phone Number"
                  name="CellPhoneNo"
                  value={patient.form.CellPhoneNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="envelope-o"
                  label="Email Address"
                  name="EmailAddress"
                  value={patient.form.EmailAddress} />
              </div>
              <h4>Sponsorship Information</h4>
              <div className="input-wrapper-divider"></div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="credit-card"
                  label="Sponsor"
                  name="SponsorID"
                  value={patient.form.SponsorID}
                  options={sponsors} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="globe"
                  label="Preferred Language"
                  name="RelationID"
                  value={patient.form.RelationID}
                  options={yesAndNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Member #"
                  name="IDNo"
                  value={patient.form.IDNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Card Serial #"
                  name="UnNo"
                  value={patient.form.UnNo} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithDate
                  handleChange={this.handleDateChange("SponsorValidFrom")}
                  label="Sponsor Valid From"
                  name="SponsorValidFrom"
                  value={patient.form.SponsorValidFrom} />
              <InputWrapperWithDate
                  handleChange={this.handleDateChange("SponsorValidTo")}
                  label="Sponsor Valid To"
                  name="SponsorValidTo"
                  value={patient.form.SponsorValidTo} />
              <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="credit-card"
                  label="Accepts CoPay"
                  name="SponsorAcceptsCoPay"
                  value={patient.form.SponsorAcceptsCoPay}
                  options={yesAndNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="money"
                  label="Status"
                  name="SponsorStatus"
                  value={patient.form.SponsorStatus}
                  options={yesAndNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="envelope-o"
                  label="Validity Expiry SMS Alert"
                  name="SponsorSMSAlert"
                  value={patient.form.SponsorSMSAlert}
                  options={yesAndNo} />
              </div>
              <h4>Contact Persons</h4>
              <div className="input-wrapper-divider"></div>
              <div className="grid-container">
                  <div className="input-wrapper-group grid-box" ref="contactGrid"></div>
              </div>
              <h4>General Information</h4>
              <div className="input-wrapper-divider"></div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="building"
                  label="Clinic"
                  name="ClinicID"
                  value={patient.form.ClinicID}
                  options={clinics} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="OPD #"
                  name="OPDNo"
                  value={patient.form.OPDNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="envelope-o"
                  label="SMS Alert"
                  name="SMSAlert"
                  value={patient.form.SMSAlert}
                  options={yesAndNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="circle-thin"
                  label="Status"
                  name="PatientStatusID"
                  value={patient.form.PatientStatusID}
                  options={patientStatuses} />
                <InputWrapperWithDate
                  handleChange={this.handleDateChange("Date")}
                  label="Date"
                  name="Date"
                  value={patient.form.Date} />
              </div>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleDelete}>Delete</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default connect((state) => {
  return {
    patient: state.patient
  }
})(PatientRegistration)
