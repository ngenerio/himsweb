import {
    LOGIN_UPDATE,
    LOGIN_VALIDATE_ERROR,
    LOGIN_SERVER_ERROR,
    LOGIN_UPDATE_SUBMIT
} from '../constants'

function updateLoginFields(state, value) {
    var field = value.field
    var value = value.value

    return { ...state, login: { ...state.login, [field]: value } }
}

function serverError(state, msg) {
    var { errors } = state.login
    errors.serverError = msg

    return { ...state, login: { ...state.login, errors } }
}

export const initState = {
    login: {
        username: '',
        password: '',
        submitting: false,
        errors: {}
    },
    forgot: {

    }
}

export default function accountReducer(state = initState, action) {
    switch (action.type) {
        case LOGIN_UPDATE:
            return updateLoginFields(state, action.payload.value)
        case LOGIN_VALIDATE_ERROR:
            return { ...state,
                login: { ...state.login, ...action.payload }
            }
        case LOGIN_SERVER_ERROR:
            return serverError(state, action.payload.message)
        case LOGIN_UPDATE_SUBMIT:
            return { ...state,
                login: { ...state.login, submitting: action.payload.submitting }
            }
        default:
            return state
    }
}
