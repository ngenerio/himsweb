import _ from 'lodash'
import { SET_LOCAL_USER } from '../constants'

function setUser(user) {
    if (_.isEmpty(user)) {
        return { availableUser: false }
    }

    return { availableUser: true, ...user }
}

export default function setLocalUser(state = {}, action) {
    switch (action.type) {
        case SET_LOCAL_USER:
            return setUser(action.payload.user)
        default:
            return state
    }
}
