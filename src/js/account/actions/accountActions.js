import { setLocalUser } from '../../account/actions/userActions'
import {
  LOGIN_UPDATE,
  LOGIN_VALIDATE_ERROR,
  LOGIN_SERVER_ERROR,
  LOGIN_UPDATE_SUBMIT
} from '../constants'
import http from '../../libs/http'

export function loginFormUpdate(updateValue) {
  return {
    type: LOGIN_UPDATE,
    payload: { value: updateValue }
  }
}

export function loginServerError(message) {
  return {
    type: LOGIN_SERVER_ERROR,
    payload: { message: message }
  }
}
export function loginValidationErrors(errors) {
  return {
    type: LOGIN_VALIDATE_ERROR,
    payload: { errors: errors }
  }
}

export function loginUpdateSubmit(submitting) {
  return {
    type: LOGIN_UPDATE_SUBMIT,
    payload: { submitting }
  }
}

export function login() {
  return (dispatch, getState) => {
    const { username, password } = getState().account.login

    return http.post(
      'Users/Login',
      { username: username, password: password },
      { uiProgress: true}
    )
    .then((response) => {
      var { Data: [{ AccessToken, Profile }] } = response.data
      window.config.token = AccessToken
      dispatch(setLocalUser(Profile))
      return response
    }, (errorResponse) => {
      const responseStatus = (errorResponse.status / 100) | 0
      let message = 'Cannot login. Provide valid username and password'

      if (responseStatus !== 4) {
        message = 'Please contact admin or try again later. :('
      }

      dispatch(loginServerError(message))
      return errorResponse
    })
  }
}
