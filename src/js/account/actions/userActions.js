import { SET_LOCAL_USER } from '../constants.js'

export function setLocalUser(user = {}) {
  return {
    type: SET_LOCAL_USER,
    payload: {
      user
    }
  }
}
