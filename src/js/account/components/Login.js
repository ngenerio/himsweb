import { Component, PropTypes } from 'react'
import _ from 'lodash'
import { replace } from 'react-router-redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import {
  loginFormUpdate,
  login,
  loginValidationErrors,
  loginUpdateSubmit
} from '../actions/accountActions'
import Icon from '../../core/components/Icon.js'

const validate = (values) => {
  const errors = {}

  if (!values.username) {
    errors.username = 'Username is required'
  } else if (!values.password) {
    errors.password = 'Password is required'
  }

  return errors
}

export default class Login extends Component {
  constructor () {
    super()
    this.handleLoginSubmit = this.handleLoginSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (evt) {
    var target = evt.target
    const field = target.name
    const value = target.value

    this.props.dispatch(loginFormUpdate({field, value}))
  }

  handleLoginSubmit (evt) {
    evt.preventDefault()
    const {
      account: { login: { username, password } },
      dispatch
    } = this.props
    const errors = validate({username, password})
    dispatch(loginValidationErrors(errors))

    if (!_.isEmpty(errors)) return

    dispatch(loginUpdateSubmit(true))
    this.props.dispatch(login())
    .then((response) => {
      dispatch(loginUpdateSubmit(false))

      if (response.code === 0) {
        dispatch(replace('/'))
      }
    })
  }

  render () {
    const { account:
      { login: { username, password, errors, submitting } },
      dispatch
    } = this.props

    return (
      <div className="login">
        <div className="form-header">
            <h2>HIMS Login</h2>
        </div>
        <div className="form-content">
          <form onSubmit={this.handleLoginSubmit}>

            <div className="input-wrapper-container login">
              <div className="input-wrapper">
                <div className="input-icon">
                  <Icon size="small" name="user" />
                </div>
                <input type="text" name="username" value={username} placeholder="Username" onChange={this.handleChange} />
              </div>
              { errors.username &&
                <div className="error">
                  {errors.username}
                </div>
              }
            </div>

            <div className="input-wrapper-container login">
              <div className="input-wrapper">
                <div className="input-icon">
                  <Icon size="small" name="lock" />
                </div>
                <input type="password" name="password" value={password} placeholder="Password" onChange={this.handleChange} />
              </div>
              { errors.password &&
                <div className="error">
                  {errors.password}
                </div>
              }
            </div>

            <div className="input-wrapper-container login">
              <div className="input-wrapper">
                <button disabled={submitting} className="form-button" type="submit">
                  { submitting ? 'Loging in' : 'Login' }
                </button>
              </div>
              { errors.serverError &&
                <div className="error">
                    {errors.serverError}
                </div>
              }
            </div>
            <div className="form-links">
              <p>Don't have Account? Contact Admin</p>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

Login.propTypes = {
    dispatch: PropTypes.func,
    account: PropTypes.object
}
