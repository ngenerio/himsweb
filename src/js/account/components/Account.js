import { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

export default class Account extends Component {
  render() {
    const { dispatch, account } = this.props
    const children = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, { dispatch, account })
    })

    return (
      <div className="account-holder">
        {children}
      </div>
    )
  }
}

export default connect((state) => {
  return {
    account: state.account
  }
})(Account)
