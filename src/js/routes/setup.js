import React from 'react'
import { Route } from 'react-router'

import Clinics from '../setups/components/Clinics'
import AllSetups from '../setups/components/AllSetups'
import Facility from '../setups/components/Facility'
import AccountSetup from '../setups/components/Account'
import Clinic from '../setups/components/Clinic'
import User from '../setups/components/User'
import Region from '../setups/components/Region'
import Regions from '../setups/components/Regions'
import District from '../setups/components/District'
import Districts from '../setups/components/Districts'
import Towns from '../setups/components/Towns'
import ServicePlace from '../setups/components/ServicePlace'
import ServicePlaces from '../setups/components/ServicePlaces'
import MaritalStatus from '../setups/components/MaritalStatus'
import Occupation from '../setups/components/Occupation'
import Occupations from '../setups/components/Occupations'
import Religion from '../setups/components/Religion'
import Sponsor from '../setups/components/Sponsor'
import Sponsors from '../setups/components/Sponsors'
import SponsorDepartment from '../setups/components/SponsorDepartment'
import SponsorMember from '../setups/components/SponsorMember'

export default (
  <Route path="setups" component={AllSetups}>
    <Route path="facility" component={Facility} />
    <Route path="account" component={AccountSetup} />
    <Route path="user" component={User} />
    <Route path="region" component={Region} />
    <Route path="region/:id" component={Region} />
    <Route path="regions" component={Regions} />
    <Route path="district" component={District} />
    <Route path="district/:id" component={District} />
    <Route path="districts" component={Districts} />
    <Route path="towns" component={Towns} />
    <Route path="serviceplace" component={ServicePlace} />
    <Route path="clinic" component={Clinic} />
    <Route path="clinic/:id" component={Clinic} />
    <Route path="clinics" component={Clinics} />
    <Route path="maritalstatus" component={MaritalStatus} />
    <Route path="occupation" component={Occupation} />
    <Route path="occupation/:id" component={Occupation} />
    <Route path="occupations" component={Occupations} />
    <Route path="religion" component={Religion} />
    <Route path="sponsor" component={Sponsor} />
    <Route path="sponsors" component={Sponsors} />
    <Route path="sponsors/departments" component={SponsorDepartment} />
    <Route path="sponsors/members" component={SponsorMember} />
    <Route path="serviceplace" component={ServicePlace} />
    <Route path="serviceplaces" component={ServicePlaces} />
  </Route>
)
