import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from '../containers/App'
import Home from '../containers/Home'
import Account from '../account/components/Account'
import Login from '../account/components/Login'
import ChangePassword from '../account/components/ChangePassword'
import PatientRegistration from '../core/components/PatientRegistration'
import ServiceRequest from '../core/components/ServiceRequest'
import setupRoutes from './setup'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="account" component={Account}>
      <Route path="login" component={Login} />
      <Route path="changepassowrd" component={ChangePassword} />
    </Route>
    <Route path="registration/patient" component={PatientRegistration} />
    <Route path="servicerequest" component={ServiceRequest} />
    {setupRoutes}
  </Route>
)
