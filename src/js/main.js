import React from 'react'
import $ from 'jquery'
import 'nanoscroller'
window.React = React

import { browserHistory } from 'react-router'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'

import '../scss/styles.scss'
import { store } from './store'
import Root from './containers/Root'

const history = syncHistoryWithStore(browserHistory, store)

render(
  <Root store={store} history={history} />,
  document.getElementById('main-container')
)
