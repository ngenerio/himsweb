import axios from 'axios'
import config from './config'
import { showProgress, hideProgress } from '../core/actions/progressActions'
import { dispatch } from '../store'

const headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

export function request(type, url, data = {}, requestConfig = {}) {
  let promise = null
  let tempHeaders = { ...headers }
  let token

  if (window.config.token) {
    token = window.config.token
    tempHeaders['Authorization'] = `${token.TokenType} ${token.Token}`
  }

  if (requestConfig.uiProgress) dispatch(showProgress())

  if (type === 'post' || type === 'put') {
    promise = axios[type](`${config.API_ENDPOINT}/${url}`, data, { headers: tempHeaders })
  } else if (type === 'get') {
    promise = axios.get(`${config.API_ENDPOINT}/${url}`, { headers: tempHeaders })
  }

  return promise
  .then((response) => {
    if (requestConfig.uiProgress) dispatch(hideProgress())
    return {
        status: response.status,
        data: response.data,
        code: 0,
    }
  })
  .catch((response) => {
    if (requestConfig.uiProgress) dispatch(hideProgress())
    return Promise.reject({
      status: response.status,
      data: 'An expected error occured',
      code: 1
    })
  })
}

export default {
    get(url, data = {}, config) {
        return request('get', url, data, config)
    },

    post(url, data = {}, config) {
        return request('post', url, data, config)
    },

    put(url, data = {}, config) {
        return request('put', url, data, config)
    }
}
