const config = {
  API_ENDPOINT: 'http://infotechhims.com/api'
}

window.config = window.config || {}
export default { ...config, ...window.config }
