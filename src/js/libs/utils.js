export function getNowToISO () {
  return (new Date()).toISOString()
}

export function setGridProps () {
  $('.htCore').css('width', '930px')
  $('.wtHider').css('width', '930px')
  $('.grid-box.handsontable').css({
    'width': '930px'
  })
  $('.grid-container').css({
    'width': '930px',
    'height': '150px',
    'overflow': 'auto'
  })
}

export function createAction (type, payload) {
  if (!payload) return { type }
  return { type, payload }
}

export function getAllMenus (menus) {
  return menus
}
