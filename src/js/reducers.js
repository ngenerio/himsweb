import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'
import core from './core/reducers/coreReducers'
import menu from './core/reducers/menuReducers'
import user from './account/reducers/userReducers'
import account from './account/reducers/accountReducers'
import setups from './setups/reducers/setupReducers'
import progress from './core/reducers/progressReducers'
import alert from './core/reducers/alertReducers'
import patient from './core/reducers/patientReducers'
import serviceRequest from './core/reducers/serviceRequest'
import sidebar from './core/reducers/sidebar'

export default combineReducers({
  user,
  menu,
  account,
  routing,
  core,
  setups,
  progress,
  alert,
  patient,
  serviceRequest,
  sidebar
})
