import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { browserHistory } from 'react-router'
import { routerMiddleware } from 'react-router-redux'
import rootReducer from '../reducers'

export const configureStore = (initialState) => {
  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(thunk, routerMiddleware(browserHistory)),
      window.devToolsExtension && process.env.NODE_ENV === 'development' ?
      window.devToolsExtension() :
      f => f
    )
  )

  if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('./', () => {
      const nextRootReduer = require('../reducers').default
      store.replaceReducer(nextRootReduer)
    })
  }

  return store
}

export const store = configureStore()
export const dispatch = store.dispatch
export const getState = store.getState
