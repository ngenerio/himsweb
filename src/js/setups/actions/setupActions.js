import { pushPath } from 'react-router-redux'
import http from '../../libs/http'
import { setFacility, setGeneralInformation } from '../../core/actions/coreActions'
import { hideProgress, showProgress } from '../../core/actions/progressActions'
import { alert } from '../../core/actions/alertActions'
import { getNowToISO } from '../../libs/utils'
import {
  SETUP_FACILILTY,
  UPDATE_FACILITY_FORM,
  CLEAR_ALL_FACILITY,
  SET_CLINIC_FORM,
  SET_CLINIC_TYPES,
  UPDATE_CLINIC_FORM,
  RECEIVE_USER_FORM,
  UPDATE_USER_FORM,
  UPDATE_OCC_FORM,
  CLEAR_OCC_FORM,
  UPDATE_REGION_FORM,
  CLEAR_SPONSOR_FORM,
  UPDATE_SPONSOR_FORM,
  RECEIVE_SPONSOR_DATA,
  UPDATE_TOWN_FORM,
  CLEAR_TOWN_FORM,
  ADD_USERS_CLINICS,
  ADD_SPONSOR_EXEMPTED,
  RECEIVE_SERVICE_TYPES,
  RECEIVE_TOWN,
  RECEIVE_ALL_TOWNS,
  RECEIVE_ALL_DISTRICTS,
  RECEIVE_ALL_REGIONS,
  RECEIVE_REGION,
  RECEIVE_DISTRICT,
  SAVE_ATTENDANCE,
  UPDATE_DISTRICT_FORM,
  CLEAR_DISTRICT,
  CLEAR_REGION,
  RECEIVE_SPONSORS,
  RECEIVE_OCCUPATIONS,
  RECEIVE_OCCUPATION,
  CLEAR_SERVICEPLACE_FORM,
  CLEAR_CLINIC_FORM,
  RECEIVE_SERV_DATA,
  SAVE_SERVICE_CLINICS,
  SAVE_SERVICE_TYPES,
  RECEIVE_SERVICE_PLACES,
  UPDATE_SERVICE_PLACE
} from '../constants'

export function setupFacility(facility) {
  return {
    type: SETUP_FACILILTY,
    payload: {
      facility
    }
  }
}

export function facilityFormUpdate(payload) {
  return {
    type: UPDATE_FACILITY_FORM,
    payload
  }
}

export function clearAllFacilitySetups() {
  return {
    type: CLEAR_ALL_FACILITY
  }
}

export function setClinicFormData(clinicFormData) {
  return {
    type: SET_CLINIC_FORM,
    payload: {
      formData: clinicFormData
    }
  }
}

export function setClinicTypes(types) {
  return {
    type: SET_CLINIC_TYPES,
    payload: {
      types
    }
  }
}

export function clinicSetupFormUpdate(val) {
  return {
    type: UPDATE_CLINIC_FORM,
    payload: val
  }
}

export function receiveUserFormData(userformData) {
  return {
    type: RECEIVE_USER_FORM,
    payload: {
      formData: userformData
    }
  }
}

export function userFormUpdate(val) {
  return {
    type: UPDATE_USER_FORM,
    payload: val
  }
}

export function clearOccupation() {
  return {
    type: CLEAR_OCC_FORM
  }
}

export function occupationFormUpdate(val) {
  return {
    type: UPDATE_OCC_FORM,
    payload: val
  }
}

export function regionFormUpdate(val) {
  return {
    type: UPDATE_REGION_FORM,
    payload: val
  }
}

export function clearRegion() {
  return {
    type: CLEAR_REGION
  }
}

export function clearUserForm() {
  return {
    type: CLEAR_USER_FORM
  }
}

export function clearClinicForm() {
  return {
    type: CLEAR_CLINIC_FORM
  }
}

export function clearSponsor() {
  return {
    type: CLEAR_SPONSOR_FORM
  }
}

export function updateSponsorForm(val) {
  return {
    type: UPDATE_SPONSOR_FORM,
    payload: val
  }
}

export function receiveSponsorFormData(sponsorFormData) {
  return {
    type: RECEIVE_SPONSOR_DATA,
    payload: {
      formData: sponsorFormData
    }
  }
}

export function townFormUpdate(payload) {
  return {
    type: UPDATE_TOWN_FORM,
    payload
  }
}

export function clearTown() {
  return {
    type: CLEAR_TOWN_FORM
  }
}

export function saveUserClinincs(clinics) {
  return {
    type: ADD_USERS_CLINICS,
    payload: clinics
  }
}

export function addExemptedService(exempted) {
  return {
    type: ADD_SPONSOR_EXEMPTED,
    payload: exempted
  }
}

export function saveFacilitySetup(facility) {
  return (dispatch, getState) => {
    const {
      user
    } = getState()
    facility.UpdateUserID = user.User.Id
    return http.put('Facility/EditFacility', facility, { uiProgress: true })
    .then((response) => {
      dispatch(alert({
        type: 'success',
        message: 'Saved succesfully',
      }))
      dispatch(setFacility(facility))
      dispatch(setupFacility(facility))
      dispatch(setGeneralInformation())
    })
  }
}

export function getClinicFormData() {
  return (dispatch, getState) => {
    dispatch(showProgress())
    return Promise.all([
      http.get('ClinicSetups/ClinicFormData'),
      http.get('ClinicSetups/GetClinicTypes')
    ])
    .then((responses) => {
      const [{
        data: {
          Data: [clinicFormData]
        }
      }, {
        data: {
          Data: types
        }
      }] = responses
      dispatch(hideProgress())
      dispatch(setClinicTypes(types))
      dispatch(setClinicFormData(clinicFormData))
    }, () => {
      dispatch(hideProgress())
      dispatch(alert({
        type: 'error',
        message: 'Could not fetch clinic types data. Try again'
      }))
    })
  }
}

export function saveClinic(clinic) {
  return (dispatch, getState) => {
    const {
      setups: {
        clinic: {
          form
        }
      },
      user,
      core
    } = getState()

    form.CreateUserID = user.User.Id
    form.ServicePointTypeID = 1
    form.CreateTime = getNowToISO()
    form.FacilityID = core.facility.Id

    return http.post('ClinicSetups/AddNewClinic', form, { uiProgress: true })
    .then((response) => {
      dispatch(alert({
        type: 'success',
        message: 'Saved clinic data succesfully'
      }))
      dispatch(pushPath('/'))
    }, (errorResponse) => {
      const message = errorResponse.status / 100 | 0 === 4 ?
        'Ensure data entered is correct' :
        'Could not save'

      dispatch(alert({
        type: 'error',
        message
      }))
    })
  }
}

export function getUserFormData() {
  return (dispatch, getState) => {
    return http.get('Users/FormData', {}, { uiProgress: true })
    .then((response) => {
      const {
        Data: [userformData]
      } = response.data
      dispatch(receiveUserFormData(userformData))
      return response
    }, (error) => {
      dispatch(alert({
        type: 'error',
        message: 'Could not fetch user form data'
      }))
      return error
    })
  }
}

export function receiveServiceTypes(serviceTypes) {
  return {
    type: RECEIVE_SERVICE_TYPES,
    payload: serviceTypes
  }
}

export function getSponsorFormData() {
  return (dispatch, getState) => {
    dispatch(showProgress())
    return Promise.all([
      http.get('SponsorSetups/SponsorFormData'),
      http.get('ServicesSetups/GetServiceTypes')
    ])
    .then((responses) => {
      dispatch(hideProgress())
      try {
        const [{
          data: {
            Data: [sponsorFormData]
          }
        }, {
          data: {
            Data: serviceTypes
          }
        }] = responses
        dispatch(receiveSponsorFormData(sponsorFormData))
        dispatch(receiveServiceTypes(serviceTypes))
        return sponsorFormData
      } catch (e) {
        dispatch(alert({
          type: 'error',
          message: 'Could not parse form response'
        }))
      }

    }, (error) => {
      dispatch(hideProgress())
      dispatch(alert({
        type: 'error',
        message: 'Could not sponsor setup form'
      }))
    })
  }
}

export function saveUser(user, id) {
  return (dispatch, getState) => {

  }
}

export function saveOccupation(occupation, id) {
  return (dispatch, getState) => {

  }
}

export function saveRegion(region, id) {
  return (dispatch, getState) => {

  }
}

export function saveSponsor(sponsor, id) {
  return (dispatch, getState) => {

  }
}

export function getTownInformation(id) {
  return (dispatch, getState) => {
    return http.get(`GeneralSetups/GetTown//${id}`, {}, { uiProgress: true })
    .then((response) => {
      const {
        Data: [townInformation]
      } = response.data
      dispatch({
        type: RECEIVE_TOWN,
        payload: townInformation
      })
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error fetching town information'
      }))
    })
  }
}

export function getAllTowns() {
  return (dispatch, getState) => {
    return http.get('GeneralSetups/GetAllTowns', {}, { uiProgress: true })
    .then((response) => {
      const {
        Data: towns
      } = response.data
      dispatch({
        type: RECEIVE_ALL_TOWNS,
        payload: towns
      })
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error fetching towns'
      }))
      return err
    })
  }
}

export function getAllRegions() {
  return (dispatch, getState) => {
    return http.get('GeneralSetups/GetAllRegions', {}, { uiProgress: true })
    .then((response) => {
      const {
        Data: regions
      } = response.data
      dispatch({
        type: RECEIVE_ALL_REGIONS,
        payload: regions
      })
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error fetching regions'
      }))
      return err
    })
  }
}

export function getAllDistricts() {
  return (dispatch, getState) => {
    return http.get('GeneralSetups/GetAllDistricts', {}, { uiProgress: true })
    .then((response) => {
      const {
        Data: districts
      } = response.data
      dispatch({
        type: RECEIVE_ALL_DISTRICTS,
        payload: districts
      })
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error fetching districts'
      }))
      return err
    })
  }
}

export function getRegionInfo(id) {
  return (dispatch, getState) => {
    dispatch(showProgress())
    return Promise.all([
      http.get(`GeneralSetups/GetRegion/${id}`),
      http.get('GeneralSetups/GetAllDistricts')
    ])
    .then((responses) => {
      dispatch(hideProgress())
      let [{
        data: {
          Data: [regionInfo]
        }
      }, {
        data: {
          Data: districts
        }
      }] = responses

      districts = districts.filter((district) => district.RegionID === regionInfo.Id)
      regionInfo.Districts = districts
      dispatch({
        type: RECEIVE_REGION,
        payload: regionInfo
      })
      return {
        code: 0,
        data: responses
      }
    }, (err) => {
      dispatch(hideProgress())
      dispatch(alert({
        type: 'error',
        message: 'Error fetching region information'
      }))
      return err
    })
  }
}

export function getDistrictInfo(id) {
  return (dispatch, getState) => {
    dispatch(showProgress())
    return Promise.all([
      http.get(`GeneralSetups/GetDistrict/${id}`),
      http.get('GeneralSetups/GetAllTowns')
    ])
    .then((responses) => {
      dispatch(hideProgress())
      let [{
        data: {
          Data: [districtInfo]
        }
      }, {
        data: {
          Data: towns
        }
      }] = responses

      towns = towns.filter((town) => town.DistrictID === districtInfo.Id)
      districtInfo.Towns = towns
      dispatch({
        type: RECEIVE_DISTRICT,
        payload: districtInfo
      })
      return {
        code: 0,
        data: responses
      }
    }, (err) => {
      dispatch(hideProgress())
      dispatch(alert({
        type: 'error',
        message: 'Error fetching district information'
      }))
      return err
    })
  }
}

export function saveTown(town, id) {
  return (dispatch, getState) => {

  }
}

export function saveClinicAttendanceType(data) {
  return {
    type: SAVE_ATTENDANCE,
    payload: data
  }
}

export function districtFormUpdate(val) {
  return {
    type: UPDATE_DISTRICT_FORM,
    payload: val
  }
}

export function saveDistrict(district, id) {
  return (dispatch, getState) => {

  }
}

export function clearDistrict() {
  return {
    type: CLEAR_DISTRICT
  }
}

export function getAllSponsors() {
  return (dispatch, getState) => {
    return http.get('SponsorSetups/GetSponsors', {}, { uiProgress: true })
    .then((response) => {
      let {
        Data: sponsors
      } = response.data
      sponsors = sponsors.map((val) => val.Sponsor)
      dispatch({
        type: RECEIVE_SPONSORS,
        payload: sponsors
      })
      return sponsors
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error fetching sponsorship information'
      }))
      return err
    })
  }
}

export function getOccupationInfo(id) {
  return (dispatch, getState) => {
    return http.get(`GeneralSetups/GetOccupation/${id}`, {}, { uiProgress: true })
    .then((response) => {
      const {
        Data: [occupation]
      } = response.data
      dispatch({
        type: RECEIVE_OCCUPATION,
        payload: occupation
      })
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error fetching occupation information'
      }))
      return err
    })
  }
}

export function getAllOccupations() {
  return (dispatch, getState) => {
    return http.get('GeneralSetups/GetAllOccupations', {}, { uiProgress: true })
    .then((response) => {
      const {
        Data: occupations
      } = response.data
      dispatch({
        type: RECEIVE_OCCUPATIONS,
        payload: occupations
      })
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error fetching occupations information'
      }))
      return err
    })
  }
}

export function servicePlaceFormUpdate(val) {
  return {
    type: UPDATE_SERVICE_PLACE,
    payload: val
  }
}

export function clearServicePlace() {
  return {
    type: CLEAR_SERVICEPLACE_FORM
  }
}

export function saveServicePlace(servicePlace, id) {
  return (dispatch, getState) => {

  }
}

export function getServicePlaceFormData() {
  return (dispatch, getState) => {
    return http.get('ClinicSetups/ServicePlaceFormData', {}, { uiProgress: true })
    .then((response) => {
      const { Data: [servicePlaceFormData] } = response.data
      dispatch({
        type: RECEIVE_SERV_DATA,
        payload: servicePlaceFormData
      })
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error getting service place form data'
      }))
      return err
    })
  }
}

export function saveServiceClinics(clinics) {
  return {
    type: SAVE_SERVICE_CLINICS,
    payload: clinics
  }
}

export function saveServiceTypes(types) {
  return {
    type: SAVE_SERVICE_TYPES,
    payload: types
  }
}

export function getAllServicePlaces() {
  return (dispatch, getState) => {
    return http.get('ClinicSetups/GetServicePlaces', {}, { uiProgress: true })
    .then((response) => {
      let { Data: servicePlaces } = response.data
      servicePlaces = servicePlaces.map((val) => val.ServicePlace)
      dispatch({
        type: RECEIVE_SERVICE_PLACES,
        payload: servicePlaces
      })
      return response
    }, (err) => {
      dispatch(alert({
        type: 'error',
        message: 'Error get all service places'
      }))
      return err
    })
  }
}
