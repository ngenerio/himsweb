import { push } from 'react-router-redux'
import { dispatch } from '../../store'

export function goToAllRegions(evt) {
  evt && evt.preventDefault()
  dispatch(push('/setups/regions'))
}

export function goToAllDistricts(evt) {
  evt && evt.preventDefault()
  dispatch(push('/setups/districts'))
}

export function goToAllTowns(evt) {
  evt && evt.preventDefault()
  dispatch(push('/setups/towns'))
}
