import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import { saveReligion, religionFormUpdate, clearReligion, getReligionInfo } from '../actions/setupActions'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'

export default class Religion extends Component {
  constructor () {
    super()
    this.handleInputChange = this.handleInputChange.bind(this)
    this.save = (evt) => {
      evt.preventDefault()
      this.props.dispatch(saveReligion(this.props.setups.religion.form))
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearReligion())
    }
  }

  handleInputChange (evt) {
    const target = evt.target
    let { name: field, value } = target

    if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
      value = value === 'true' ? true : false
    }

    this.props.dispatch(religionFormUpdate({field, value}))
  }

  componentDidMount () {
    if (this.props.routeParams.id) {
      this.props.dispatch(getReligionInfo(this.props.routeParams.id))
    }
    this.setState({
      yesAndNo: [{ value: true, label: 'Yes' }, { value: false, label: 'No' }]
    })
  }

  render () {
    const { yesAndNo } = this.state || {}
    const { setups: { religion } } = this.props

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
              Religion Setup
          </h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>
              Religion Information
            </p>
          </div>
          <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Name"
                  name="Name"
                  value={religion.form.Name} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Code"
                  name="Code"
                  value={religion.form.Code} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="thumbs-o-up"
                  label="Is Active"
                  name="IsActive"
                  value={religion.form.IsActive}
                  options={yesAndNo} />
              </div>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToAllReligions}>All Religions</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToAllOccupations}>All Occupations</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
