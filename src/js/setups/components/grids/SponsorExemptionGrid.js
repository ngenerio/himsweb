import { Component, PropTypes } from 'react'
import http from '../../../libs/http'
import { alert } from '../../../core/actions/alertActions'
import { addExemptedService } from '../../actions/setupActions'
import { setGridProps } from '../../../libs/utils'

export default class SponsorExemptionGrid extends Component {
  renderGrid (props) {
    props = props || this.props
    let services = props.services || []
    let types = props.types || []
    let currentCol = ''

    const createOptions = (types, desc, data) => {
      return {
        data: (data && data.length) ? data : null,
        startRows: 3,
        maxRows: 20,
        minSpareRows: 2,
        colHeaders: ['Service Type', 'Service Description'],
        columns: [
          {
            type: 'autocomplete',
            source: types.map((val) => val.Name),
            strict: false
          },
          {
            type: 'autocomplete',
            source(query, proc) {
              let data = []
              if (currentCol) {
                for (let j = 0; j < types.length; j++) {
                  if (currentCol === types[j].Name) {
                    for (let k = 0; k < desc.length; k++) {
                      if (desc[k].ServiceTypeID === types[j].Id) {
                        if (query &&
                          (desc[k].Name.indexOf(query) || desc[k].Name.startsWith(query))) {
                          data.push(desc[k])
                        } else {
                          data.push(desc[k])
                        }
                      }
                    }
                    break
                  }
                }
              }

              proc(data.map((val) => val.Name))
            },
            strict: false
          }
        ]
      }
    }

    let newServices = [ ...services ]
    let exempted = props.exempted || []

    exempted = exempted.map((val) => [val.type, val.service] )
    const grid = this.grid = new Handsontable(this.refs.grid, createOptions(types, newServices, exempted))

    grid.addHook('afterChange', (changes) => {
      let [ [row, column, , newVal] ] = changes
      currentCol = !!newVal ? newVal : currentCol

      let exemptedData = grid.getData().map((val) => {
        return { type: val[0], service: val[1] }
      })
      this.props.dispatch(addExemptedService(exemptedData))
    })

    setGridProps()
  }

  componentDidMount () {
    this.renderGrid()
  }

  render () {
    return (
      <div className="input-wrapper-group grid-box nano-content" ref="grid"></div>
    )
  }
}
