import { Component, PropTypes } from 'react'
import {  saveUserClinincs } from '../../actions/setupActions'
import { setGridProps } from '../../../libs/utils'

export default class ClinicGrid extends Component {
  componentDidMount () {
    const data = this.props.data || []
    const selected = this.props.selected || []
    const handsonData = data.map((value) => {
      let selectedValue = false
      for (let i = 0; i < selected.length; i++) {
        if (selected[i].ClinicID === value.Id) {
          selectedValue = true
          break
        }
      }
      return { Name: value.Name, Id: value.Id, Selected: selectedValue }
    })
    const handsontableOptions = {
      data: handsonData,
      colHeaders: ['Select', 'Clinic'],
      columns: [
        { data: 'Selected', type: 'checkbox' },
        { data: 'Name' }
      ]
    }

    const grid = new Handsontable(this.refs.grid, handsontableOptions)
    Handsontable.hooks.add('afterChange', () => {
      var saveClinics = grid.getData()
      saveClinics = saveClinics.filter((val) => val[0]).map((val) => {
        for (let i = 0; i < data.length; i++) {
          if (data[i].Name === val[1]) {
            return { ClinicID: data[i].Id }
          }
        }
      })
      this.props.dispatch(saveUserClinincs(saveClinics))
    }, grid)

    setGridProps()
  }

  render () {
    return (
      <div className="input-wrapper-group grid-box nano-content" ref="grid"></div>
    )
  }
}
