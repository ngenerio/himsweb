import { Component, PropTypes } from 'react'
import {  saveServiceTypes } from '../../actions/setupActions'
import { setGridProps } from '../../../libs/utils'

export default class ServiceTypesGrid extends Component {
  renderGrid () {
    const data = this.props.data || []
    const selected = this.props.selected || []
    const handsonData = data.map((value) => {
      let selectedValue = false
      for (let i = 0; i < selected.length; i++) {
        if (selected[i].ServicePlaceTypeID === value.Id) {
          selectedValue = true
          break
        }
      }
      return { Name: value.Name, Id: value.Id, Selected: selectedValue }
    })

    const handsontableOptions = {
      data: handsonData,
      colHeaders: ['Include', 'Name'],
      minSpareRows: 2,
      columns: [
        { data: 'Selected', type: 'checkbox' },
        { data: 'Name' }
      ]
    }

    const grid = this.grid = new Handsontable(this.refs.grid, handsontableOptions)
    grid.addHook('afterChange', () => {
      var serviceTypes = grid.getData()
      serviceTypes = serviceTypes.filter((val) => val[0]).map((val) => {
        for (let i = 0; i < data.length; i++) {
          if (data[i].Name === val[1]) {
            return { ServicePlaceTypeID: data[i].Id, Name: data[i].Name }
          }
        }
      })
      this.props.dispatch(saveServiceTypes(serviceTypes))
    })
    setGridProps()
  }

  componentDidMount () {
    this.renderGrid()
  }

  componentDidUpdate () {
    this.grid && this.grid.destroy()
    this.renderGrid()
  }

  render () {
    return (
      <div className="input-wrapper-group grid-box nano-content" ref="grid"></div>
    )
  }
}
