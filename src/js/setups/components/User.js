import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import DatePicker from 'react-datepicker'
import Tabs from 'react-simpletabs'
import { getUserFormData, saveUser, userFormUpdate, clearUserForm } from '../actions/setupActions'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'
import InputWrapperWithDate from '../../core/components/InputWrapperWithDate'
import ClinicGrid from './grids/ClinicsGrid'


export default class User extends Component {
  constructor () {
    super()

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleDateChange = this.handleDateChange.bind(this)
    this.save = (evt) => {
      evt.preventDefault()
      this.props.dispatch(saveUser())
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearUserForm())
    }

    this.goToAllUsers = (evt) => {
      evt.preventDefault()
      this.props.dispatch(push('/setups/users/all'))
    }
  }

  handleInputChange (evt) {
    const target = evt.target
    let { name: field, value } = target

    if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
      value = value === 'true' ? true : false
    }

    this.props.dispatch(userFormUpdate({field, value}))
  }

  handleDateChange (name) {
    return (date) => {
      this.props.dispatch(userFormUpdate({field: name, value: date}))
    }
  }

  componentDidMount () {
    const { setups, directorates } = this.props.core
    const getValues = (val) => {
      return { label: val.Name, value: val.Id }
    }

    this.props.dispatch(getUserFormData())
    .then(() => {
      const { setups: { user }, core: { clinics } } = this.props


      this.setState({
        userTypes: user.formData.UserTypes.map(getValues),
        genderGroups: user.formData.GenderGroups.map(getValues),
        userCategories:  user.formData.UserCategories.map(getValues),
        yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}],
        clinics: user.formData.Clinics
      })
    })
  }

  render () {
    const {
      userTypes,
      genderGroups,
      userCategories,
      yesAndNo,
      clinics
    } = this.state || {}
    const { setups: { user }, dispatch } = this.props

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
              Users Setup
          </h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>
                Add new user
            </p>
          </div>
          <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="First Name"
                  name="FrstName"
                  value={user.form.FrstName} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Other Name"
                  name="OtherName"
                  value={user.form.OtherName} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Last Name"
                  name="LastName"
                  value={user.form.LastName} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="user"
                  label="User ID"
                  name="Id"
                  value={user.form.Id} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="lock"
                  label="User Password"
                  name="Password"
                  value={user.form.Password} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="lock"
                  label="Confirm Password"
                  name="ConfirmPassword"
                  value={user.form.ConfirmPassword} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="envelope-o"
                  label="Email Address"
                  name="EmailAddress"
                  value={user.form.EmailAddress} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="envelope-o"
                  label="Alternate Email Address"
                  name="AlternateEmailAddress"
                  value={user.form.AlternateEmailAddress} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="mars"
                  label="Gender Group"
                  name="GenderGroupID"
                  value={user.form.GenderGroupID}
                  options={genderGroups} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="phone"
                  label="Primary Cell Number"
                  name="PrimaryCellNo"
                  value={user.form.PrimaryCellNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="phone"
                  label="Secondary Cell Number"
                  name="SecondaryCellNo"
                  value={user.form.SecondaryCellNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="circle-o"
                  label="User Type"
                  name="UserTypeID"
                  value={user.form.UserTypeID}
                  options={userTypes} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="circle-o"
                  label="User Category"
                  name="UserCategoryID"
                  value={user.form.UserCategoryID}
                  options={userCategories} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="circle-o"
                  label="User Level"
                  name="UserLevelID"
                  value={user.form.UserLevelID}
                  options={userCategories} />
                <InputWrapperWithDate
                  handleChange={this.handleDateChange("AccountExpiryDate")}
                  label="Expires on"
                  name="AccountExpiryDate"
                  value={user.form.AccountExpiryDate} />
              <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="lock"
                  label="Block User"
                  name="IsLockedOut"
                  value={user.form.IsLockedOut}
                  options={yesAndNo} />
              </div>
              <Tabs>
                <Tabs.Panel title="User Access Levels">
                  <div className="grid-container">User Access Level</div>
                </Tabs.Panel>
                <Tabs.Panel title="User Clinics">
                  <div className="grid-container">
                    <ClinicGrid dispatch={dispatch} data={clinics} selected={user.form.Clinics}/>
                  </div>
                </Tabs.Panel>
              </Tabs>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToAllUsers}>View All User</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button">User Roles</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button">External Doctors</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
