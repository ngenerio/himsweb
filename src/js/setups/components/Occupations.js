import { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { push } from 'react-router-redux'
import ReactDOM from 'react-dom'
import { setGridProps } from '../../libs/utils'
import { getAllOccupations } from '../actions/setupActions'

export default class Occupation extends Component {
  componentDidMount () {
    const { dispatch } = this.props

    dispatch(getAllOccupations())
    .then((response) => {
      if (response.code === 0) {
        const { setups: { occupations } } = this.props
        const handsontableOptions = {
          data: occupations,
          colHeaders: ['Id', 'Name', 'Is Active'],
          columns: [
            {
              data: 'Id',
              startRows: 2,
              renderer (instance, td, row, col, prop, value, cellProperties) {
                const link = (
                  <Link
                    to={`/setups/occupation/${value}`}
                    onClick={() => { dispatch(push(`/setups/occupation/${value}`)) }}>
                    {value}
                  </Link>
                )
                Handsontable.Dom.empty(td)
                ReactDOM.render(link, td)
              }
            },
            { data: 'Name' },
            { data: 'IsActive' }
          ]
        }

        const grid = new Handsontable(this.refs.grid, handsontableOptions)
        setGridProps()
      }
    })
  }


  render () {
    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>Occupation</h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>All Occupations</p>
          </div>
          <div className="form-content">
            <div className="grid-container">
              <div className="input-wrapper-group grid-box" ref="grid"></div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
