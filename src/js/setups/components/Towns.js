import { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import ReactDOM from 'react-dom'
import { push } from 'react-router-redux'
import { getAllTowns } from '../actions/setupActions'
import { goToAllRegions, goToAllDistricts, goToAllTowns } from '../actions/utils'

export default class Towns extends Component {
  componentDidMount () {
    this.props.dispatch(getAllTowns())
    .then((response) => {
      if (response.code === 0) {
        const { setups: { towns } } = this.props
        const handsontableOptions = {
          data: towns,
          colHeaders: ['Id', 'Name', 'Code', 'District Id'],
          startRows: 2,
          minSpareRows: 1,
          columns: [
            {
              data: 'Id',
              renderer (instance, td, row, col, prop, value, cellProperties) {
                if (value) {
                  var districtId = _.find(towns, { Id: value }).DistrictID
                  const link = (
                    <Link
                      to={`/setups/district/${districtId}`}
                      onClick={() => { dispatch(push(`/setups/district/${districtId}`)) }}>
                      {value}
                    </Link>
                  )
                  Handsontable.Dom.empty(td)
                  ReactDOM.render(link, td)
                } else {
                  Handsontable.renderers.TextRenderer.apply(this, arguments);
                }
              }
            },
            { data: 'Name' },
            { data: 'Code' },
            { data: 'DistrictID' }
          ]
        }

        const grid = new Handsontable(this.refs.grid, handsontableOptions)
        $('.htCore').css('width', '930px')
        $('.wtHider').css('width', '930px')
      }
    })
  }


  render () {
    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
            Towns Information
          </h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>All Towns</p>
          </div>
          <div className="form-content">
            <div className="grid-container">
              <div className="input-wrapper-group grid-box" ref="grid"></div>
            </div>
            <div className="input-wrapper-group input-nonflex">
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllRegions}>All Regions</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllDistricts}>All Districts</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllTowns}>All Towns</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
