import { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import ReactDOM from 'react-dom'
import { getAllSponsors } from '../actions/setupActions'

export default class Sponsors extends Component {
  componentDidMount () {
    const { dispatch } = this.props
    dispatch(getAllSponsors())
    .then((response) => {
      if (response.code === 0) {
        const { setups: { sponsors } } = this.props
        const handsontableOptions = {
          data: sponsors,
          startRows: 2,
          colHeaders: ['Id', 'Sponsor Type', 'Sponsor Name', 'Address', 'Credit Limit'],
          columns: [
            {
              data: 'Id',
              renderer (instance, td, row, col, prop, value, cellProperties) {
                const link = (
                  <Link
                    to={`/setups/region/${value}`}
                    onClick={() => { dispatch(pushPath(`/setups/region/${value}`)) }}>
                    {value}
                  </Link>
                )
                Handsontable.Dom.empty(td)
                ReactDOM.render(link, td)
              }
            },
            { data: 'SponsorType' },
            { data: 'Name' },
            { data: 'Address' },
            { data: 'CreditLimit' }
          ]
        }

        const sponsorGrid = new Handsontable(this.refs.sponsorGrid, handsontableOptions)
        $('.htCore').css('width', '930px')
        $('.wtHider').css('width', '930px')
      }
    })
  }

  render () {
    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>Regions Information</h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>All Regions</p>
          </div>
          <div className="form-content">
            <div className="grid-container">
              <div className="input-wrapper-group grid-box" ref="sponsorGrid"></div>
            </div>
            <div className="input-wrapper-group input-nonflex">
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllRegions}>All Regions</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllDistricts}>All Districts</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllTowns}>All Towns</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
