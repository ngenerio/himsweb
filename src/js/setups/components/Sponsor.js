import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import {
  getSponsorFormData,
  updateSponsorForm,
  saveSponsor,
  clearSponsor
} from '../actions/setupActions'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'
import SponsorExemptionGrid from './grids/SponsorExemptionGrid'

export default class Sponsor extends Component {
  constructor (props) {
    super(props)
    this.state = {}

    this.handleInputChange = this.handleInputChange.bind(this)
    this.save = (evt) => {
      evt.preventDefault()
      this.props.dispatch(saveSponsor())
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearSponsor())
    }

    this.goToAllSponsors = () => {
      this.props.dispatch(push('/setups/sponsors'))
    }

    this.goToDepartments = () => {
      this.props.dispatch(pushPath('/setups/sponsors/departments'))
    }

    this.goToMembers = () => {
      this.props.dispatch(pushPath('/setups/sponsors/members'))
    }
  }

  handleInputChange (evt) {
    evt.preventDefault()
    const { target } = evt
    let { name: field, value } = target

    if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
      value = value === 'true' ? true : false
    }

    this.props.dispatch(updateSponsorForm({field, value}))
  }

  componentDidMount() {
    const { dispatch } = this.props
    const getValues = (val) => {
        return { label: val.Name, value: val.Id }
    }

    dispatch(getSponsorFormData())
    .then(() => {
      const { setups: { sponsor } } = this.props || { setups: {} }

      if (sponsor && sponsor.formData) {
        this.setState({
          types: sponsor.formData.SponsorTypes.map(getValues),
          categories: sponsor.formData.SponsorCategories.map(getValues),
          services: sponsor.formData.Services.filter((val) => val.IsActive ).map((srv) => {
            return {
              Name: srv.Name,
              Id: srv.Id,
              IsActive: srv.IsActive,
              ServiceTypeID: srv.ServiceTypeID,
              ServicePlaceID: srv.ServicePlaceID,
              ServiceCategoryID: srv.ServiceCategoryID
            }
          }),
          feegroups: sponsor.formData.FeeGroups.map(getValues),
          paymentTypes: sponsor.formData.PaymentTypes.map(getValues),
          yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}]
        })
      }
    })
  }

  render () {
    const { setups: { sponsor }, dispatch } = this.props
    const {
      types,
      categories,
      services,
      feegroups,
      paymentTypes,
      yesAndNo
    } = this.state || {}

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
            Sponsor Setup
          </h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>
              Sponsor Information
            </p>
          </div>
          <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Name"
                  name="Name"
                  value={sponsor.form.Name} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="phone"
                  label="Telephone Number"
                  name="TelNo"
                  value={sponsor.form.TelNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="fax"
                  label="Fax"
                  name="Fax"
                  value={sponsor.form.Fax} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="envelope-o"
                  label="Email Address"
                  name="EmailAddress"
                  value={sponsor.form.EmailAddress} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="map-marker"
                  label="Postal Address"
                  name="PostalAddress"
                  value={sponsor.form.PostalAddress} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="globe"
                  label="Website"
                  name="Website"
                  value={sponsor.form.Website} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="credit-card"
                  label="Sponsor Type"
                  name="SponsorTypeID"
                  value={sponsor.form.SponsorTypeID}
                  options={types} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Credit Limit"
                  name="CreditLimit"
                  value={sponsor.form.CreditLimit} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Outstanding Balance"
                  name="OutStandingBalance"
                  value={sponsor.form.OutStandingBalance} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="credit-card"
                  label="Allow Co-Payment"
                  name="AllowCoPay"
                  value={sponsor.form.AllowCoPay}
                  options={yesAndNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="credit-card"
                  label="Enforce Sponsorship Expiry"
                  name="EnforceSponsorshipExpiry"
                  value={sponsor.form.EnforceSponsorshipExpiry}
                  options={yesAndNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="credit-card"
                  label="Sponsor Category"
                  name="SponsorCategoryID"
                  value={sponsor.form.SponsorCategoryID}
                  options={categories} />
              </div>
              <h4>Services Exempted From Sponsorship</h4>
              <div className="input-wrapper-divider"></div>
              <div className="grid-container">
                { (types && services) ?
                  <SponsorExemptionGrid
                      services={services}
                      types={sponsor.types}
                      dispatch={dispatch}
                      exempted={sponsor.form.ExemptedServices} />
                  : null
                }
              </div>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToAllSponsors}>View All Sponsors</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToDepartments}>Sponsor Departments</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToMembers}>Member Details</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
