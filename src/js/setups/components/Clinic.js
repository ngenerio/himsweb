import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import {
  getClinicFormData,
  saveClinic,
  clinicSetupFormUpdate,
  saveClinicAttendanceType,
  clearClinicForm
} from '../actions/setupActions'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'

export default class Clinic extends Component {
  constructor () {
    super()
    this.handleInputChange = this.handleInputChange.bind(this)
    this.save = (evt) => {
      evt.preventDefault()
      this.props.dispatch(saveClinic())
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearClinicForm())
    }

    this.goToAllClinics = (evt) => {
      evt.preventDefault()
      this.props.dispatch(push('/setups/clinics'))
    }
  }

  handleInputChange (evt) {
      const target = evt.target
      let { name: field, value } = target

      if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
          value = value === 'true' ? true : false
      }

      this.props.dispatch(clinicSetupFormUpdate({field, value}))
  }

  componentDidMount () {
    const { setups, directorates } = this.props.core
    const getValues = (val) => {
        return { label: val.Name, value: val.Id }
    }

    this.props.dispatch(getClinicFormData())
    .then(() => {
        const { setups: { clinic } } = this.props

        this.setState({
          clinicTypes: clinic.types.map(getValues),
          genderGroups: clinic.formData.GenderGroups.map(getValues),
          ageGroups:  clinic.formData.AgeGroups.map(getValues),
          opdnumbering: clinic.formData.ClinicOPDNumberings.map(getValues),
          directorates: clinic.formData.Directorates.map(getValues),
          patientTypes: clinic.formData.PatientStatuses.map(getValues),
          servicePointTypes:  clinic.formData.ServicePointTypes.map(getValues),
          yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}]
        })

        const handsontableOptions = {
          colHeaders: ['Description', 'Default'],
          startRows: 2,
          maxRows: 10,
          minSpareRows: 1,
          columns: [
              { type: 'autocomplete', source: clinic.types.map((val) => val.Name) },
              { type: 'checkbox' }
          ]
        }

        const attendanceGrid = new Handsontable(this.refs.attendanceGrid, handsontableOptions)
        attendanceGrid.addHook('afterChange', (changes) => {
          let data = attendanceGrid.getData()

          data = data.map((val) => {

          })
          this.props.dispatch(saveClinicAttendanceType(data))
        })
        $('.htCore').css('width', '930px')
        $('.wtHider').css('width', '930px')
      })
    }

  render () {
    const {
        clinicTypes,
        genderGroups,
        ageGroups,
        opdnumbering,
        directorates,
        patientTypes,
        servicePointTypes,
        yesAndNo
    } = this.state || {}
    const { setups: { clinic } } = this.props

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
              Clinics Setup
          </h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>Add new clinic</p>
          </div>
            <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Name"
                  name="Name"
                  value={clinic.form.Name} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Prefix"
                  name="Prefix"
                  value={clinic.form.Prefix} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Start OPD #"
                  name="StartOPDNo"
                  value={clinic.form.StartOPDNo} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Digits After Prefix"
                  name="DigitsNo"
                  value={clinic.form.DigitsNo} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Year Part Digits"
                  name="YearDigitsNo"
                  value={clinic.form.YearDigitsNo} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="calendar-o"
                  label="Age Group"
                  name="AgeGroupID"
                  value={clinic.form.AgeGroupID} options={ageGroups} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="mars"
                  label="Gender Group"
                  name="GenderGroupID"
                  value={clinic.form.GenderGroupID} options={genderGroups} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="h-square"
                  label="Patient Type"
                  name="PatientStatusID"
                  value={clinic.form.PatientStatusID} options={patientTypes} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="building"
                  label="Directorate"
                  name="DirectorateID"
                  value={clinic.form.DirectorateID} options={directorates} />
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="When should patient be assigned OPD #"
                  name="ClinicOPDNoSystemID"
                  value={clinic.form.ClinicOPDNoSystemID} options={opdnumbering} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="thumbs-o-up"
                  label="Active"
                  name="IsActive"
                  value={clinic.form.IsActive} options={yesAndNo} />
              </div>
              <h4>Attendance Types</h4>
              <div className="input-wrapper-divider"></div>
              <div className="grid-container">
                <div className="input-wrapper-group grid-box" ref="attendanceGrid"></div>
              </div>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                    <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                    <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                 <div className="input-wrapper-container">
                    <button className="form-button" onClick={this.goToAllClinics}>View All Clinics</button>
                </div>
                <div className="input-wrapper-container">
                    <button className="form-button">Consulting Room</button>
                </div>
                <div className="input-wrapper-container">
                    <button className="form-button">External Clinics</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
