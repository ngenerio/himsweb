import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import Tabs from 'react-simpletabs'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'
import InputWrapperWithDate from '../../core/components/InputWrapperWithDate'
import ServicePlaceClinicsGrid from './grids/ServicePlaceClinicsGrid'
import ServiceTypesGrid from './grids/ServiceTypesGrid'
import {
  servicePlaceFormUpdate,
  clearServicePlace,
  saveServicePlace,
  getServicePlaceFormData
} from '../actions/setupActions'
import { alert } from '../../core/actions/alertActions'

export default class ServicePlace extends Component {
  constructor () {
    super()
    this.handleInputChange = this.handleInputChange.bind(this)

    this.goToAllServicePlaces = (evt) => {
      evt.preventDefault()
      this.props.dispatch(push('/setups/serviceplaces'))
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearServicePlace())
    }

    this.save = (evt) => {
      const { dispatch, setups: { servicePlace } } = this.props
      evt.preventDefault()

      if (this.props.routeParams.id) {
        dispatch(saveServicePlace(servicePlace.form, this.props.routeParams.id))
      } else {
        dispatch(saveServicePlace(servicePlace.form))
      }
    }
  }

  componentDidMount () {
    const getValues = (val) => {
      return { label: val.Name, value: val.Id }
    }

    this.props.dispatch(getServicePlaceFormData())
    .then((resp) => {
      if (resp.code !== 0) return

      const { setups: { servicePlace } } = this.props
      this.setState({
        ageGroups: servicePlace.formData.AgeGroups.map(getValues),
        genderGroups: servicePlace.formData.GenderGroups.map(getValues),
        patientStatuses: servicePlace.formData.PatientStatuses.map(getValues),
        clinics: servicePlace.formData.Clinics.map(getValues),
        services: servicePlace.formData.Services.map(getValues),
        servicePlaceTypes: servicePlace.formData.ServicePlaceTypes.map(getValues),
        yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}]
      })
    })
  }

  handleInputChange (evt) {
    const target = evt.target
    let { name: field, value } = target

    if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
      value = value === 'true' ? true : false
    }

    this.props.dispatch(servicePlaceFormUpdate({field, value}))
  }

  render () {
    const { setups: { servicePlace } } = this.props
    const {
      ageGroups,
      genderGroups,
      patientStatuses,
      clinics = [],
      services,
      servicePlaceTypes = [],
      yesAndNo
    } = this.state || {}

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
            HIMS Service Place
          </h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>
              HIMS Service Place Information
            </p>
          </div>
          <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Name of Service Place"
                  name="Name"
                  value={servicePlace.form.Name} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="mars"
                  label="Gender Category"
                  name="GenderCategoryID"
                  value={servicePlace.form.GenderCategoryID} options={genderGroups}/>
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Age Category"
                  name="AgeGroupID"
                  value={servicePlace.form.AgeGroupID} options={ageGroups}/>
              </div>
              <div className="input-wrapper-group">
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="quote-left"
                  label="Default Service"
                  name="DefaultServiceID"
                  value={servicePlace.form.DefaultServiceID} options={services} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="quote-left"
                  label="Patient Type"
                  name="PatientStatusID"
                  value={servicePlace.form.PatientStatusID} options={patientStatuses}/>
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="quote-left"
                  label="Is Active"
                  name="IsActive"
                  value={servicePlace.form.IsActive} options={yesAndNo}/>
              </div>
              {servicePlaceTypes.length ?
                <div className="input-wrapper-group">
                {
                  servicePlace.formData.ServicePlaceTypes.map((val, index) => {
                    return (
                      <InputWrapperWithSelect
                        key={index}
                        handleChange={this.handleDefaultServicesChange}
                        icon="quote-left"
                        label={val.Name}
                        name={val.Name}
                        value={servicePlace.form[val.Name]}
                        options={yesAndNo} />
                    )
                  })
                }
                </div> : null
              }
              <Tabs>
                <Tabs.Panel title="Clinics">
                  <div className="grid-container nano">
                    <ServicePlaceClinicsGrid
                      data={servicePlace.formData.Clinics}
                      selected={servicePlace.form.ClinicIDs}
                      dispatch={this.props.dispatch} />
                  </div>
                </Tabs.Panel>
                <Tabs.Panel title="Service Types">
                  <div className="grid-container nano">
                    <ServiceTypesGrid
                      data={servicePlace.formData.ServicePlaceTypes}
                      selected={servicePlace.form.ServicePlaceTypeIDs}
                      dispatch={this.props.dispatch} />
                  </div>
                </Tabs.Panel>
              </Tabs>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToAllServicePlaces}>View Service Places</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

ServicePlace.propTypes = {
    core: PropTypes.object,
    setups: PropTypes.object
}
