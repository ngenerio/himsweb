import { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { push } from 'react-router-redux'
import ReactDOM from 'react-dom'
import { getAllRegions } from '../actions/setupActions'
import { goToAllRegions, goToAllDistricts, goToAllTowns } from '../actions/utils'

export default class Regions extends Component {
    componentDidMount () {
      const { dispatch } = this.props
      dispatch(getAllRegions())
      .then((response) => {
        if (response.code === 0) {
          const { setups: { regions } } = this.props
          const handsontableOptions = {
            data: regions,
            colHeaders: ['Id', 'Name', 'Code', 'Is Active'],
            startRows: 2,
            columns: [
              {
                data: 'Id',
                renderer (instance, td, row, col, prop, value, cellProperties) {
                  const link = (
                    <Link
                      to={`/setups/region/${value}`}
                      onClick={() => { dispatch(push(`/setups/region/${value}`)) }}>
                      {value}
                    </Link>
                  )
                  Handsontable.Dom.empty(td)
                  ReactDOM.render(link, td)
                }
              },
              { data: 'Name' },
              { data: 'Code' },
              { data: 'IsActive' }
            ]
          }

          const grid = new Handsontable(this.refs.grid, handsontableOptions)
          $('.htCore').css('width', '930px')
          $('.wtHider').css('width', '930px')
        }
      })
    }


  render () {
    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>Regions Information</h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>All Regions</p>
          </div>
          <div className="form-content">
            <div className="grid-container">
              <div className="input-wrapper-group grid-box" ref="grid"></div>
            </div>
            <div className="input-wrapper-group input-nonflex">
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllRegions}>All Regions</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllDistricts}>All Districts</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllTowns}>All Towns</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
