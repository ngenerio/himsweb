import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import { Link } from 'react-router'
import ReactDOM from 'react-dom'
import { getAllDistricts } from '../actions/setupActions'
import { goToAllRegions, goToAllDistricts, goToAllTowns } from '../actions/utils'

export default class Districts extends Component {
  constructor (props) {
    super(props)
    this.goToRegion = this.goToRegion.bind(this)
  }

  goToRegion () {
    this.props.dispatch(push('/setups/region'))
  }

  componentDidMount () {
    const { dispatch } = this.props

    dispatch(getAllDistricts())
    .then((response) => {
      if (response.code === 0) {
        const { setups: { districts } } = this.props
        const handsontableOptions = {
          data: districts,
          startRows: 2,
          minSpareRows: 2,
          colHeaders: ['Id', 'Name', 'Code', 'Region Id'],
          columns: [
            {
              data: 'Id',
              renderer (instance, td, row, col, prop, value, cellProperties) {
                if (value) {
                  var regionId = _.find(towns, { Id: value }).RegionID
                  const link = (
                      <Link
                          to={`/setups/region/${regionId}`}
                          onClick={() => { dispatch(pushPath(`/setups/region/${regionId}`)) }}>
                          {value}
                      </Link>
                  )
                  Handsontable.Dom.empty(td)
                  ReactDOM.render(link, td)
                } else {
                  Handsontable.renderers.TextRenderer.apply(this, arguments);
                }
              }
            },
            { data: 'Name' },
            { data: 'Code' },
            { data: 'RegionID' }
          ]
        }

        const grid = new Handsontable(this.refs.grid, handsontableOptions)
        $('.htCore').css('width', '930px')
        $('.wtHider').css('width', '930px')
      }
    })
  }


  render () {
    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>Districts Information</h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>All Districts</p>
          </div>
          <div className="form-content">
            <div className="grid-container">
              <div className="input-wrapper-group grid-box" ref="grid"></div>
            </div>
            <div className="input-wrapper-group input-nonflex">
              <div className="input-wrapper-container">
                <button className="form-button" onClick={this.goToRegion}>Region</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllDistricts}>All Districts</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={goToAllTowns}>All Towns</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
