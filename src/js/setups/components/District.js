import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import { Link } from 'react-router'
import cx from 'classnames'
import ReactDOM from 'react-dom'
import { saveDistrict, districtFormUpdate, clearDistrict, getDistrictInfo } from '../actions/setupActions'
import {
    goToAllDistricts,
    goToAllRegions,
    goToAllTowns
} from '../actions/utils'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'

export default class Region extends Component {
  constructor () {
    super()
    this.handleInputChange = this.handleInputChange.bind(this)
    this.save = (evt) => {
      evt.preventDefault()
      this.props.dispatch(saveDistrict())
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearDistrict())
    }

    this.state = {
      showGrid: false
    }
  }

  handleInputChange (evt) {
    const target = evt.target
    let { name: field, value } = target

    if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
      value = value === 'true' ? true : false
    }

    this.props.dispatch(districtFormUpdate({field, value}))
  }

  renderGrid (data) {
    const handsontableOptions = {
      data: data || [],
      startRows: 2,
      minSpareRows: 2,
      colHeaders: ['Id', 'Name', 'Code', 'Is Active'],
      columns: [
        { data: 'Id' },
        { data: 'Name' },
        { data: 'Code' },
        {
            data: 'IsActive',
            type: 'autocomplete',
            source: ['Yes', 'No']
        }
      ]
    }

    const grid = new Handsontable(this.refs.grid, handsontableOptions)
    grid.addHook('afterChange', (changes) => {
      let [[row, prop, oldVal, newVal]] = changes
      let townData = grid.getData()


      if (prop === 'Name' && !townData[row][3]) {
          grid.setDataAtCell(row, 3, 'Yes')
      }

      townData = grid.getData().filter((val) => val[1])
      .map((val) => {
          return {
              Id: val[0],
              Name: val[1],
              Code: val[2],
              IsActive: val[3] === 'Yes' ? true : false
          }
      })
    })
    $('.htCore').css('width', '930px')
    $('.wtHider').css('width', '930px')
    $('.grid-box.handsontable').css({
        'height': '200px',
        'overflow': 'hidden'
    })
    this.setState({ showGrid: true })
  }

  componentDidMount () {
    const { dispatch } = this.props
    const getValues = (val) => {
      return { label: val.Name, value: val.Id }
    }

    if (this.props.routeParams.id) {
      dispatch(getDistrictInfo(this.props.routeParams.id))
      .then((response) => {
        if (response.code === 0) {
          const { setups: { district } } = this.props
          this.renderGrid(district.form.Towns)
        }
      })
    } else {
      this.renderGrid()
    }

    this.setState({
      yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}]
    })
  }

  render () {
    const { yesAndNo } = this.state || {}
    const { setups: { district } } = this.props

    return (
      <div className="content-container">
        <div className="content-panel-title">
            <h2>District</h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>
                District Information
            </p>
          </div>
          <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="font"
                  label="Name"
                  name="Name"
                  value={district.form.Name} />
                <InputWrapper
                  handleChange={this.handleInputChange}
                  icon="hashtag"
                  label="Code"
                  name="Code"
                  value={district.form.Code} />
                <InputWrapperWithSelect
                  handleChange={this.handleInputChange}
                  icon="thumbs-o-up"
                  label="Is Active"
                  name="IsActive"
                  value={district.form.IsActive} options={yesAndNo} />
              </div>
              <div className={cx(this.state.showGrid ? 'is-active' : 'is-inactive')} >
                <h4>Districts under Region - {district.form.Name}</h4>
                <div className="input-wrapper-divider"></div>
                <div className="grid-container">
                    <div className="input-wrapper-group grid-box" ref="grid"></div>
                </div>
              </div>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={goToAllRegions}>All Regions</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={goToAllDistricts}>All Districts</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={goToAllTowns}>All Towns</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
