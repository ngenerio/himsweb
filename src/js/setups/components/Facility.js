import { Component, PropTypes } from 'react'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'
import InputWrapperWithDate from '../../core/components/InputWrapperWithDate'
import {
    facilityFormUpdate,
    clearAllFacilitySetups,
    saveFacilitySetup,
    setupFacility
} from '../actions/setupActions'
import { alert } from '../../core/actions/alertActions'

export default class Facility extends Component {
  constructor () {
      super()
      this.handleInputChange = this.handleInputChange.bind(this)
      this.handleDateChange = this.handleDateChange.bind(this)

      this.handleClearAll = (evt) => {
          evt.preventDefault()
          this.props.dispatch(clearAllFacilitySetups())
      }

      this.save = (evt) => {
          const { dispatch, setups: { facility } } = this.props
          evt.preventDefault()
          dispatch(saveFacilitySetup(facility))
          .then(undefined, (errResponse) => {
              dispatch(alert({
                  message: 'Could not save facility information',
                  type: 'error'
              }))
          })
      }
  }

  handleDateChange (name) {
      return (date) => {
          this.props.dispatch(facilityFormUpdate({field: name, value: date}))
      }
  }

  componentDidMount () {
      const { setups } = this.props.core
      const getValues = (val) => {
          return { label: val.Name, value: val.Id }
      }

      this.props.dispatch(setupFacility(this.props.core.facility))
      this.setState({
          countries: setups.Countries.map(getValues),
          districts: setups.Districts.map(getValues),
          towns: setups.Towns.map(getValues),
          regions: setups.Regions.map(getValues),
          opdnumbering: setups.OPDNumberingSystems.map(getValues),
          facilitylevels: setups.FacilityLevels.map(getValues),
          characterTypes: setups.CharacterTypes.map(getValues),
          ownershipTypes: setups.OwnershipTypes.map(getValues),
          yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}]
      })
  }

  handleInputChange (evt) {
      const target = evt.target
      let { name: field, value } = target

      if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
          value = value === 'true' ? true : false
      }

      this.props.dispatch(facilityFormUpdate({field, value}))
  }

  render () {
    const { core: { general }, setups: { facility }, user } = this.props
    const {
        countries,
        districts,
        towns,
        regions,
        opdnumbering,
        facilitylevels,
        characterTypes,
        ownershipTypes,
        yesAndNo,
    } = this.state || {}

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
            Facility Setup for {`${general.facilityName}, ${general.country}`}
          </h2>
        </div>
        <div className="content-header">
          <p>
            All the necessary information concerning the hospital in this case: {general.facilityName} is stored, edited and viewed here.
          </p>
        </div>
          <div className="content-content">
            <div className="form-content">
              <form onSubmit={this.save}>
                <div className="input-wrapper-group">
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="font"
                    label="Name of Hospital"
                    name="Name"
                    value={facility.Name} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="map-marker"
                    label="Postal Address"
                    name="PostalAddress"
                    value={facility.PostalAddress} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="quote-left"
                    label="Motto"
                    name="Motto"
                    value={facility.Motto} />
                </div>
                <div className="input-wrapper-group">
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="phone"
                    label="Telephone"
                    name="TelNo"
                    value={facility.TelNo} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="fax"
                    label="Fax"
                    name="Fax"
                    value={facility.Fax} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="envelope-o"
                    label="Email"
                    name="Email"
                    value={facility.Email} />
                </div>
                <div className="input-wrapper-group">
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="globe"
                    label="Website"
                    name="Website"
                    value={facility.Website} />
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="map-marker"
                    label="Country"
                    name="CountryID"
                    value={facility.CountryID} options={countries} />
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="map-marker"
                    label="Region"
                    name="RegionID"
                    value={facility.RegionID} options={regions} />
                </div>
                <div className="input-wrapper-group">
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="map-marker"
                    label="District"
                    name="DistrictID"
                    value={facility.DistrictID} options={districts} />
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="map-marker"
                    label="Towns"
                    name="TownID"
                    value={facility.TownID} options={towns} />
                  <InputWrapperWithDate
                    handleChange={this.handleDateChange("Date")}
                    label="Date Established"
                    name="Date"
                    value={facility.Date} />
                </div>
                <div className="input-wrapper-divider"></div>
                <div className="input-wrapper-group">
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="hashtag"
                    label="Prefix"
                    name="Prefix"
                    value={facility.Prefix} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="hashtag"
                    label="Start OPD"
                    name="StartOPDNo"
                    value={facility.StartOPDNo} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="hashtag"
                    label="Digits After Prefix #"
                    name="DigitsNoAfterPrefix"
                    value={facility.DigitsNoAfterPrefix} />
                </div>
                <div className="input-wrapper-group">
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="calendar-times-o"
                    label="Year Digits"
                    name="YearDigitsNo"
                    value={facility.YearDigitsNo} />
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="circle"
                    label="Health Facility Level"
                    name="FacilityLevelID"
                    value={facility.FacilityLevelID} options={facilitylevels} />
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="hashtag"
                    label="OPD Numbering System"
                    name="OPDNoSystemID"
                    value={facility.OPDNoSystemID} options={opdnumbering} />
                </div>
                <div className="input-wrapper-group">
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="hashtag"
                    label="OPD Prefix Character Type"
                    name="PrefixCharType"
                    value={facility.PrefixCharType} options={characterTypes} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="hashtag"
                    label="Prefix Length"
                    name="PrefixLength"
                    value={facility.PrefixLength} />
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="thumbs-o-up"
                    label="Auto Generate OPD"
                    name="AutoGenerateOPDNo"
                    value={facility.AutoGenerateOPDNo} options={yesAndNo} />
                </div>
                <div className="input-wrapper-group">
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="thumbs-o-up"
                    label="Allow Editing of OPD"
                    name="AllowOPDNoEdit"
                    value={facility.AllowOPDNoEdit} options={yesAndNo} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="lock"
                    label="Maximum Login Attempts"
                    name="MaxLogInAttempts"
                    value={facility.MaxLogInAttempts} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="user-times"
                    label="Inactivity Timeout(Minutes)"
                    name="TimeoutTime"
                    value={facility.TimeoutTime} />
                </div>
                <div className="input-wrapper-group">
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="user"
                    label="Ownership Type"
                    name="OwnershipTypeID"
                    value={facility.OwnershipTypeID} options={ownershipTypes} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="calendar-o"
                    label="Patient Max Age"
                    name="PatientsMaxAge"
                    value={facility.PatientsMaxAge} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="unlock"
                    label="Password Expiry (Days)"
                    name="PasswordExpiryDays"
                    value={facility.PasswordExpiryDays} />
                </div>
                <div className="input-wrapper-group">
                  <InputWrapperWithSelect
                    handleChange={this.handleInputChange}
                    icon="lock"
                    label="Password Character Type"
                    name="PasswordCharType"
                    value={facility.PasswordCharType} options={characterTypes} />
                  <InputWrapper
                    handleChange={this.handleInputChange}
                    icon="lock"
                    label="Password Minimum Length"
                    name="PasswordMinLength"
                    value={facility.PasswordMinLength} />
                </div>
                <div className="input-wrapper-group input-nonflex">
                  <div className="input-wrapper-container">
                    <button className="form-button" onClick={this.save}>Save</button>
                  </div>
                  <div className="input-wrapper-container">
                    <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                  </div>
                </div>
              </form>
            </div>
        </div>
      </div>
    )
  }
}

Facility.propTypes = {
    core: PropTypes.object,
    setups: PropTypes.object
}
