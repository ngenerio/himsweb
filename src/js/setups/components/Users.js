import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import { Link } from 'react-router'
import ReactDOM from 'react-dom'
import { getAllUsers } from '../actions/setupActions'
import { setGridProps } from '../../libs/utils'

export default class Districts extends Component {
  constructor (props) {
    super(props)
    this.goToExternalDocs = this.goToExternalDocs.bind(this)
  }

  goToExternalDocs (evt) {
    evt.preventDefault()
    this.props.dispatch(push('/setups/externaldocs'))
  }

  goToRegion () {
    this.props.dispatch(push('/setups/region'))
  }

  componentDidMount () {
    const { dispatch } = this.props

    dispatch(getAllUsers())
    .then((response) => {
      if (response.code === 0) {
        let { setups: { users } } = this.props
        users = users.map((user) => {
          user.Name = user.FirstName + user.LastName
          user.Locked = user.IsLockedOut ? 'Yes' : 'No'
          return user
        })

        const handsontableOptions = {
          data: users,
          startRows: 2,
          minSpareRows: 2,
          colHeaders: [
            'Id', 'Username', 'Name', 'Locked',
            'User Role', 'Main Email Address', 'Alternate Email Address',
            'Category ID', 'Primary Cell No', 'Secondary Cell No',
            'User Type', 'User Grade', 'Gender', 'Date of Birth'
          ],
          columns: [
            {
              data: 'Id',
              renderer (instance, td, row, col, prop, value, cellProperties) {
                if (value) {
                  var userId = _.find(users, { Id: value }).Id
                  const link = (
                    <Link
                      to={`/setups/user/${userId}`}
                      onClick={() => { dispatch(push(`/setups/user/${userId}`)) }}>
                      {value}
                    </Link>
                  )
                  Handsontable.Dom.empty(td)
                  ReactDOM.render(link, td)
                } else {
                  Handsontable.renderers.TextRenderer.apply(this, arguments);
                }
              }
            },
            { data: 'UserName' },
            { data: 'Code' },
            { data: 'RegionID' }
          ]
        }

        const grid = new Handsontable(this.refs.grid, handsontableOptions)
        setGridProps()
      }
    })
  }


  render () {
    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>Users Information</h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>All Users</p>
          </div>
          <div className="form-content">
            <div className="grid-container">
              <div className="input-wrapper-group grid-box" ref="grid"></div>
            </div>
            <div className="input-wrapper-group input-nonflex">
              <div className="input-wrapper-container">
                <button className="form-button">User Roles</button>
              </div>
              <div className="input-wrapper-container">
                <button className="form-button" onClick={this.goToExternalDocs}>External Doctors</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

