import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import { saveOccupation, occupationFormUpdate, clearOccupation, getOccupationInfo } from '../actions/setupActions'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'

export default class Occupation extends Component {
  constructor () {
    super()
    this.handleInputChange = this.handleInputChange.bind(this)
    this.save = (evt) => {
      evt.preventDefault()
      const  { occupation } = this.props.setups
      const id = this.props.routeParams.id || null
      this.props.dispatch(saveOccupation(occupation.form, id))
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearOccupation())
    }

    this.goToAllOccupations = (evt) => {
      evt.preventDefault()
      this.props.dispatch(push('/setups/occupations'))
    }
  }

  handleInputChange (evt) {
      const target = evt.target
      let { name: field, value } = target

      if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
          value = value === 'true' ? true : false
      }

      this.props.dispatch(occupationFormUpdate({field, value}))
  }

  componentDidMount () {
      if (this.props.routeParams.id) {
          this.props.dispatch(getOccupationInfo(this.props.routeParams.id))
      }

      this.setState({
          yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}]
      })
  }

  render () {
    const { yesAndNo } = this.state || {}
    const { setups: { occupation } } = this.props

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>
            Occupation Setup
          </h2>
        </div>
        <div className="content-content">
           <div className="content-header">
            <p>
              Occupation Information
            </p>
          </div>
          <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper handleChange={this.handleInputChange} icon="font" label="Name" name="Name" value={occupation.form.Name} />
                <InputWrapper handleChange={this.handleInputChange} icon="hashtag" label="Id" name="Id" value={occupation.form.Id} />
                <InputWrapperWithSelect handleChange={this.handleInputChange} icon="thumbs-o-up" label="Is Active" name="IsActive" value={occupation.form.IsActive} options={yesAndNo} />
              </div>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToAllOccupations}>View All Occupations</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
