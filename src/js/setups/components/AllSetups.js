import { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

export class AllSetups extends Component {
  render () {
    const { dispatch, core, user, setups } = this.props
    const children = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, { dispatch, core, user, setups })
    })
    return (
      <div>
          { !user.availableUser ? null : children }
      </div>
    )
  }
}

export default connect((state) => {
  return {
    setups: state.setups
  }
})(AllSetups)
