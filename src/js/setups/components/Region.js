import { Component, PropTypes } from 'react'
import { push } from 'react-router-redux'
import { Link } from 'react-router'
import cx from 'classnames'
import ReactDOM from 'react-dom'
import { saveRegion, regionFormUpdate, clearRegion, getRegionInfo } from '../actions/setupActions'
import {
  goToAllDistricts,
  goToAllRegions,
  goToAllTowns
} from '../actions/utils'
import InputWrapper from '../../core/components/InputWrapper'
import InputWrapperWithSelect from '../../core/components/InputWrapperWithSelect'

export default class Region extends Component {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.goToDistrict = this.goToDistrict.bind(this)

    this.save = (evt) => {
      evt.preventDefault()
      this.props.dispatch(saveRegion())
    }

    this.handleClearAll = (evt) => {
      evt.preventDefault()
      this.props.dispatch(clearRegion())
    }

    this.state = {
      showGrid:  false
    }
  }

  goToDistrict () {
    this.props.dispatch(push('/setups/district'))
  }

  handleInputChange (evt) {
    const target = evt.target
    let { name: field, value } = target

    if (target.nodeName.toLowerCase() === 'select' && (value === 'true' || value === 'false')) {
      value = value === 'true' ? true : false
    }

    this.props.dispatch(regionFormUpdate({field, value}))
  }

  renderGrid (data) {
    const handsontableOptions = {
      data: data || [],
      colHeaders: ['Id', 'Name', 'Code', 'Is Active'],
      startRows: 2,
      minSpareRows: 2,
      columns: [
        {
          data: 'Id'
        },
        { data: 'Name' },
        { data: 'Code' },
        {
          type: 'autocomplete',
          source: ['Yes', 'No'],
          data: 'IsActive'
        }
      ]
    }

    const regionGrid = new Handsontable(this.refs.grid, handsontableOptions)
    regionGrid.addHook('afterChange', (changes) => {
      let [[row, prop, oldVal, newVal]] = changes
      let districtsData = regionGrid.getData()


      if (prop === 'Name' && !districtsData[row][3]) {
        regionGrid.setDataAtCell(row, 3, 'Yes')
      }

      districtsData = regionGrid.getData().filter((val) => val[1])
      .map((val) => {
        return {
          Id: val[0],
          Name: val[1],
          Code: val[2],
          IsActive: val[3] === 'Yes' ? true : false
        }
      })
    })
    $('.htCore').css('width', '930px')
    $('.wtHider').css('width', '930px')
    $('.grid-box.handsontable').css({
      'height': '150px',
      'overflow': 'hidden'
    })
    this.setState({ showGrid: true })
  }

  componentDidMount () {
      const { dispatch } = this.props
      if (this.props.routeParams.id) {
          dispatch(getRegionInfo(this.props.routeParams.id))
          .then((response) => {
              if (response.code === 0) {
                  const { setups: { region } } = this.props
                  this.renderGrid(region.form.Districts)
              }
          })
      } else {
          this.renderGrid()
      }

      this.setState({
          yesAndNo: [{ value: true, label: 'Yes'}, { value: false, label: 'No'}]
      })
  }

  render () {
    const { yesAndNo } = this.state || {}
    const { setups: { region } } = this.props

    return (
      <div className="content-container">
        <div className="content-panel-title">
          <h2>Region</h2>
        </div>
        <div className="content-content">
          <div className="content-header">
            <p>
              Region Information
            </p>
          </div>
          <div className="form-content">
            <form onSubmit={this.save}>
              <div className="input-wrapper-group">
                <InputWrapper handleChange={this.handleInputChange} icon="font" label="Name" name="Name" value={region.form.Name} />
                <InputWrapper handleChange={this.handleInputChange} icon="hashtag" label="Code" name="Code" value={region.form.Code} />
                <InputWrapperWithSelect handleChange={this.handleInputChange} icon="thumbs-o-up" label="Is Active" name="IsActive" value={region.form.IsActive} options={yesAndNo} />
              </div>
              <div className={cx(this.state.showGrid ? 'is-active' : 'is-inactive')} >
                <h4>Districts under Region - {region.form.Name}</h4>
                <div className="input-wrapper-divider"></div>
                <div className="grid-container">
                  <div className="input-wrapper-group grid-box" ref="grid"></div>
                </div>
              </div>
              <div className="input-wrapper-group input-nonflex">
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.save}>Save</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button btn-danger" onClick={this.handleClearAll}>Clear</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={goToAllRegions}>All Regions</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={this.goToDistrict}>District Setup</button>
                </div>
                <div className="input-wrapper-container">
                  <button className="form-button" onClick={goToAllTowns}>All Towns</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
