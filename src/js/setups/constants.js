export const SET_CLINIC_FORM = 'SET_CLINIC_FORM'
export const SET_CLINIC_TYPES = 'SET_CLINIC_TYPES'
export const UPDATE_CLINIC_FORM = 'UPDATE_CLINIC_FORM'
export const RECEIVE_USER_FORM = 'RECEIVE_USER_FORM'
export const UPDATE_USER_FORM = 'UPDATE_USER_FORM'
export const UPDATE_OCC_FORM = 'UPDATE_OCC_FORM'
export const CLEAR_OCC_FORM = 'CLEAR_OCC_FORM'
export const UPDATE_REGION_FORM = 'UPDATE_REGION_FORM'
export const CLEAR_SPONSOR_FORM = 'CLEAR_SPONSOR_FORM'
export const UPDATE_SPONSOR_FORM = 'UPDATE_SPONSOR_FORM'
export const RECEIVE_SPONSOR_DATA = 'RECEIVE_SPONSOR_DATA'
export const UPDATE_TOWN_FORM = 'UPDATE_TOWN_FORM'
export const CLEAR_TOWN_FORM = 'CLEAR_TOWN_FORM'
export const ADD_USERS_CLINICS = 'ADD_USERS_CLINICS'
export const ADD_SPONSOR_EXEMPTED = 'ADD_SPONSOR_EXEMPTED'
export const RECEIVE_SERVICE_TYPES = 'RECEIVE_SERVICE_TYPES'
export const UPDATE_FACILITY_FORM = 'UPDATE_FACILITY_FORM'
export const CLEAR_ALL_FACILITY = 'CLEAR_ALL_FACILITY'
export const SETUP_FACILILTY = 'SETUP_FACILILTY'
export const RECEIVE_TOWN = 'RECEIVE_TOWN'
export const RECEIVE_ALL_TOWNS = 'RECEIVE_ALL_TOWNS'
export const RECEIVE_REGION = 'RECEIVE_REGION'
export const RECEIVE_DISTRICT = 'RECEIVE_DISTRICT'
export const RECEIVE_ALL_REGIONS = 'RECEIVE_ALL_REGIONS'
export const RECEIVE_ALL_DISTRICTS = 'RECEIVE_ALL_DISTRICTS'
export const SAVE_ATTENDANCE = 'SAVE_ATTENDANCE'
export const UPDATE_DISTRICT_FORM = 'UPDATE_DISTRICT_FORM'
export const CLEAR_DISTRICT = 'CLEAR_DISTRICT'
export const CLEAR_REGION = 'CLEAR_REGION'
export const RECEIVE_SPONSORS = 'RECEIVE_SPONSORS'
export const RECEIVE_OCCUPATIONS = 'RECEIVE_OCCUPATIONS'
export const RECEIVE_OCCUPATION = 'RECEIVE_OCCUPATION'
export const CLEAR_USER_FORM = 'CLEAR_USER_FORM'
export const CLEAR_CLINIC_FORM = 'CLEAR_CLINIC_FORM'
export const RECEIVE_SERV_DATA = 'RECEIVE_SERV_DATA'
export const SAVE_SERVICE_CLINICS = 'SAVE_SERVICE_CLINICS'
export const SAVE_SERVICE_TYPES = 'SAVE_SERVICE_TYPES'
export const RECEIVE_SERVICE_PLACES = 'RECEIVE_SERVICE_PLACES'
export const UPDATE_SERVICE_PLACE = 'UPDATE_SERVICE_PLACE'
