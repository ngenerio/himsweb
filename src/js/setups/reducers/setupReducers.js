import {
  SETUP_FACILILTY,
  UPDATE_FACILITY_FORM,
  CLEAR_ALL_FACILITY,
  SET_CLINIC_FORM,
  SET_CLINIC_TYPES,
  UPDATE_CLINIC_FORM,
  RECEIVE_USER_FORM,
  UPDATE_USER_FORM,
  UPDATE_OCC_FORM,
  CLEAR_OCC_FORM,
  UPDATE_REGION_FORM,
  CLEAR_SPONSOR_FORM,
  UPDATE_SPONSOR_FORM,
  RECEIVE_SPONSOR_DATA,
  CLEAR_TOWN_FORM,
  UPDATE_TOWN_FORM,
  ADD_USERS_CLINICS,
  ADD_SPONSOR_EXEMPTED,
  RECEIVE_SERVICE_TYPES,
  RECEIVE_ALL_TOWNS,
  RECEIVE_ALL_DISTRICTS,
  RECEIVE_ALL_REGIONS,
  RECEIVE_TOWN,
  RECEIVE_REGION,
  RECEIVE_DISTRICT,
  UPDATE_DISTRICT_FORM,
  CLEAR_DISTRICT,
  CLEAR_REGION,
  RECEIVE_SPONSORS,
  RECEIVE_OCCUPATIONS,
  RECEIVE_OCCUPATION,
  CLEAR_USER_FORM,
  CLEAR_CLINIC_FORM,
  RECEIVE_SERV_DATA,
  RECEIVE_SERVICE_PLACES,
  SAVE_SERVICE_TYPES,
  SAVE_SERVICE_CLINICS,
  UPDATE_SERVICE_PLACE
} from '../constants'

const initState = {
  facility: {},
  clinic: { formData: {}, types: {}, form: {} },
  user: { formData: {}, form: {} },
  occupation: { form: {} },
  region: { form: {} },
  sponsor: { form: {} },
  town: { form: {} },
  towns: [],
  regions: [],
  districts: [],
  sponsors: [],
  occupations: [],
  district: { form: {} },
  servicePlace: { form: {}, formData: {} },
  servicePlaces: []
}

function updateFacilityForm (state, payload) {
  return {
    ...state,
    facility: { ...state.facility, [payload.field]: payload.value }
  }
}

function updateClinicSetup (state, payload) {
  let newState = { ...state }
  if (payload.formData) {
    newState.clinic = { ...state.clinic, formData: payload.formData }
  } else if (payload.types) {
    newState.clinic = { ...state.clinic, types: payload.types }
  }

  return newState
}

function updateForm (state, val, formType) {
  return {
    ...state,
    [formType]: {
      ...state[formType],
      form: {
        ...state[formType].form,
        [val.field]: val.value
      }
    }
  }
}

function receiveFormData (state, value, type) {
  return {
    ...state,
    [type]: {
      ...state[type],
      formData: value.formData
    }
  }
}

function clearForm (state, type) {
  return {
    ...state,
    [type]: {
      ...state[type],
      form: {}
    }
  }
}

export default function (state = initState, action) {
  switch (action.type) {
    case SETUP_FACILILTY:
      return { ...state, facility: action.payload.facility }

    case UPDATE_FACILITY_FORM:
      return updateFacilityForm(state, action.payload)

    case CLEAR_ALL_FACILITY:
      return { ...state, facility: {} }

    case SET_CLINIC_FORM:
      return updateClinicSetup(state, action.payload)

    case SET_CLINIC_TYPES:
      return updateClinicSetup(state, action.payload)

    case UPDATE_CLINIC_FORM:
      return updateForm(state, action.payload, 'clinic')

    case RECEIVE_USER_FORM:
      return receiveFormData(state, action.payload, 'user')

    case UPDATE_USER_FORM:
      return updateForm(state, action.payload, 'user')

    case UPDATE_OCC_FORM:
      return updateForm(state, action.payload, 'occupation')

    case CLEAR_OCC_FORM:
      return clearForm(state, 'occupation')

    case CLEAR_REGION:
      return clearForm(state, 'region')

    case UPDATE_REGION_FORM:
      return updateForm(state, action.payload, 'region')

    case RECEIVE_REGION:
      return { ...state, region: { form: action.payload } }

    case UPDATE_SPONSOR_FORM:
      return updateForm(state, action.payload, 'sponsor')

    case CLEAR_SPONSOR_FORM:
      return clearForm(state, 'sponsor')

    case CLEAR_USER_FORM:
      return clearForm(state, 'user')

    case CLEAR_CLINIC_FORM:
      return clearForm(state, 'clinic')

    case RECEIVE_SPONSOR_DATA:
      return receiveFormData(state, action.payload, 'sponsor')

    case RECEIVE_SERVICE_TYPES:
      return { ...state, sponsor: { ...state.sponsor, types: action.payload } }

    case UPDATE_DISTRICT_FORM:
      return updateForm(state, action.payload, 'district')

    case UPDATE_TOWN_FORM:
      return updateForm(state, action.payload, 'town')

    case CLEAR_TOWN_FORM:
      return clearForm(state, 'town')

    case CLEAR_DISTRICT:
      return clearForm(state, 'district')

    case RECEIVE_TOWN:
      return { ...state, town: { form: action.payload } }

    case RECEIVE_ALL_TOWNS:
      return { ...state, towns: action.payload }

    case RECEIVE_ALL_REGIONS:
      return { ...state, regions: action.payload }

    case RECEIVE_ALL_DISTRICTS:
      return { ...state, districts: action.payload }

    case RECEIVE_DISTRICT:
      return { ...state, district: { form: action.payload } }

    case RECEIVE_SPONSORS:
      return { ...state, sponsors: action.payload }

    case ADD_USERS_CLINICS:
      return updateForm(state, { field: 'Clinics', value: action.payload }, 'user')

    case ADD_SPONSOR_EXEMPTED:
      return updateForm(state, { field: 'ExemptedServices', value: action.payload }, 'sponsor')

    case SAVE_SERVICE_TYPES:
      return updateForm(state, { field: 'ServicePlaceTypeIDs', value: action.payload }, 'servicePlace')

    case SAVE_SERVICE_CLINICS:
      return updateForm(state, { field: 'ClinicIDs', value: action.payload }, 'servicePlace')

    case RECEIVE_OCCUPATIONS:
      return { ...state, occupations: action.payload }

    case RECEIVE_OCCUPATION:
      return { ...state, occupation: { form: action.payload } }

    case RECEIVE_SERV_DATA:
      return { ...state, servicePlace: { ...state.servicePlace, formData: action.payload } }

    case RECEIVE_SERVICE_PLACES:
      return { ...state, servicePlaces: action.payload }

    default:
      return state
  }
}
