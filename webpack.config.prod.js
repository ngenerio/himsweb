const path = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')
const HTMLWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  devtool: 'source-map',
  entry: {
    app: './src/js/main',
    vendor: ['react', 'handsontable']
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'assets/app.bundle.js'
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendor', 'assets/vendor.bundle.js'),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new HTMLWebpackPlugin({
      template: './src/index.html',
      inject: false,
      minify: {
        removeComments: true,
        collapseWhitespace: true
      }
    }),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    })
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        include: path.join(__dirname, 'src'),
        loader: 'babel'
      },
      {
        test: /\.scss$/,
        loader: 'style!css!postcss!resolve-url?sourceMap!sass?sourceMap'
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&minetype=application/font-woff&path=assets&name=[hash].[ext]'
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?path=assets&name=[hash].[ext]'
      }
    ]
  },
  postcss: [autoprefixer],
  sassLoader: {
    includePaths: [path.join(__dirname, 'node_modules/open-sans-fontface')]
  },
  resolve: {
    alias: {
      handsontable: 'handsontable/dist/handsontable.full.min.js'
    }
  }
}
